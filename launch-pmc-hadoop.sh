#!/bin/bash

set +e
hadoop fs -rm /examples/bin/pmc-hadoop
hadoop fs -rm datas/*
hadoop fs -rm output/*
hadoop fs -rmdir output
set -e

hadoop fs -put pmc-hadoop /examples/bin/
hadoop fs -put tmp/input.txt datas/input.txt

mapred pipes -files $(ls tmp/*.bin | tr '\n' ',') -conf pmc.xml -inputformat org.apache.hadoop.mapred.lib.NLineInputFormat -input datas/ -output output/

hadoop fs -tail output/part-00000 | sort -r
