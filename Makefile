#       Makefile  -  Main Makefile for PMC algorithm
#       --------------------------------------------------------
# begin                : Sept 25 2013
# copyright            : (C) 2013 2014 by Andrea Corallo
# contributed by       : Andrea Corallo <andrea_corallo@yahoo.it>

# This file is part of PMC.

# PMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# PMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with PMC.  If not, see <http://www.gnu.org/licenses/>.


#./bin/pmc -q datasets/set.int > rules.pmc
#./bin/pmc-crule -n -i rules.pmc -s'\t' datasets/set.set

SRCDIR = src
BINDIR = bin

ifeq ($(MODE),Debug)
MODEOPT = Debug
else
MODEOPT = Release
endif

VALGRINDOPT = --leak-check=full #--show-reachable=yes

default: pmc pmc-preparehdfiles pmc-mrtest pmc-crule

pmc:
	+$(MAKE) MODE=$(MODEOPT) -C $(SRCDIR) pmc
	mv $(SRCDIR)/pmc $(BINDIR)

pmc-preparehdfiles:
	+$(MAKE) -C $(SRCDIR) pmc-preparehdfiles
	mv $(SRCDIR)/pmc-preparehdfiles $(BINDIR)/

pmc-mrtest:
	+$(MAKE) -C $(SRCDIR) pmc-mrtest
	mv $(SRCDIR)/pmc-mrtest $(BINDIR)/

pmc-crule:
	+$(MAKE) -C $(SRCDIR) pmc-crule
	mv $(SRCDIR)/pmc-crule $(BINDIR)/

pmc-api:
	+$(MAKE) MODE=$(MODEOPT) -C $(SRCDIR) pmc-api
	mv $(SRCDIR)/libpmc-api.so $(BINDIR)

pmc-api-test:
	+$(MAKE) MODE=$(MODEOPT) -C $(SRCDIR) pmc-api-test
	mv $(SRCDIR)/libpmc-api.so $(BINDIR)
	mv $(SRCDIR)/pmc-api-test $(BINDIR)

doxygen:
	+$(MAKE) -C $(SRCDIR) doxygen

clean:
	+$(MAKE) -C $(SRCDIR) clean
	-rm -rf bin/*
	-rm -f valsum.out

clean_doc:
	-rm -rf doc/*

valgrind: pmc
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./bin/pmc datasets/2dchess_1000p_1000v_10dim_0err.float

valgrind-mrtest: pmc-mrtest
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./bin/pmc-mrtest

valgrind-crule: pmc-crule
	valgrind --log-file=valsum.out $(VALGRINDOPT) ./bin/pmc-crule -n -s'\t' -irules.pmc datasets/set.set

callgrind: pmc
	valgrind --tool=callgrind ./bin/pmc file100p_10dim.set && kcachegrind callgrind*
countlines:
	ls *.y *.l *.c *.cpp *.sh *.h *.cl Makefile | tr ' ' '\n' | xargs wc -l
profile:
	gprof pmc gmon.out > gmon.txt
