/**
 * @file		pmc_comb.h
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for data_chunk_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include "pmc_comb.h"

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 * Factorial function.
 *
 * @param[in] n		argument to compute
 *
 * @return		n!
 */
static size_t fact(size_t n);

/**
 * Return n!/(n-k)!
 *
 * @param[in] n		n argument
 * @param[in] k		k argument
 *
 * @return		n!/(n-k)!
 */
static size_t fact_nk(size_t n, size_t k);

/**
 * Prepare the combination already generated for our purpose.
 *
 * @param[in,out] comb		vector containing the permutation
 * @param[in] k			number of groups
 * @param[in] n			number of elements
 *
 * @return
 */
static size_t next_comb(size_t *comb, size_t k, size_t n);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern void gen_combs(size_t ***combs_ref, size_t *len_comb_ref, size_t n,
		size_t k)
{
	size_t i, len_comb;
	size_t *comb;
	size_t **combs;

	if (n > 2 * k)
		len_comb = fact_nk(n, n - k) / fact(k);
	else
		len_comb = fact_nk(n, k) / fact(n - k);

	comb = alloca(k * sizeof(*comb));
	combs = malloc(len_comb * sizeof(*combs));
	for (i = 0; i < len_comb; i++)
		combs[i] = malloc(k * sizeof(**combs));

	/* Setup comb for the initial combination */
	for (i = 0; i < k; ++i)
		comb[i] = i;

	/* Generate all the other combinations */
	for (i = 0; i < len_comb; i++) {
		memcpy(combs[i], comb, k * sizeof(*comb));
		next_comb(comb, k, n);
	}

	*combs_ref = combs;
	*len_comb_ref = len_comb;

	return;
}

extern void free_combs(size_t **combs, size_t len_comb)
{
	size_t i;

	for (i = 0; i < len_comb; i++)
		free(combs[i]);
	free(combs);

	return;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static size_t fact(size_t n)
{
	if (n == 1 || n == 0)
		return 1;
	else
		return n * fact(n - 1);
}

static size_t fact_nk(size_t n, size_t k)
{
	if (n == k)
		return 1;
	else
		return n * fact_nk(n - 1, k);
}

static size_t next_comb(size_t *comb, size_t k, size_t n)
{
	size_t i = k - 1;
	++comb[i];
	while ((i > 0) && (comb[i] >= n - k + 1 + i)) {
		--i;
		++comb[i];
	}

	if (comb[0] > n - k)
		return 0;

	for (i = i + 1; i < k; ++i)
		comb[i] = comb[i - 1] + 1;

	return 1;
}
