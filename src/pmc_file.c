/**
 * @file		pmc_file.c
 * @date		Sept 25 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Functions for file loading for PMC algorithm
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_base.h"
#include "pmc_ind.h"
#include "pmc_rules.h"

/**
 * Given a set of rules log it down to hardisk
 *
 * @param[in] rules	pointer to the struct rules_t containing the set of
 *			rules to log
 * @param[in] comb	vector with the variable indexes used by the
 *			edge
 * @param[in] len_comb	number of variable in the combo inds
 * @param[in] nout	total number of output variables
 */
void log_rules_to_hd(struct rules_t *rules, size_t *comb, size_t len_comb,
		uint32_t nout)
{
	size_t i, j;
	char filename[MAXSTRING], buf[MAXSTRING];
	FILE *pfile;

	strcpy(filename, STR(TEMPFILEDIR)
		STR(RULESFILENAME1));
	for (i = 0; i < len_comb; i++){
		sprintf(buf, STR(RULESFILENAME2), comb[i]);
		strcat(filename, buf);
	}
	strcat(filename, STR(RULESFILENAME3));


	pfile = fopen(filename, "wb");
	if (!pfile)
		error();
	for (i = 0; i < nout; i++) {
		if (rules[i].len) {
			printf("Printing rules for output number: "
				STR(SIZE_T_P) "\n", i);
			for (j = 0; j < rules[i].len; j++) {
				printf("%s", rules[i].v[j]);
				fprintf(pfile, "%s", rules[i].v[j]);
			}
		}
	}
	fclose(pfile);
}

/**
 * Count the number of lines in a file.
 *
 * @param[in] filebuf		buffer in which to perform the search
 *
 * @return			line number
 */
size_t count_lines(char *filebuf)
{
	size_t i;
	size_t lines = 0;

	for (i = 0; filebuf[i + 1]!='\0'; i++)
		if (filebuf[i]=='\n')
			lines++;
	return ++lines;
}
