/**
 * @file		pmc_api.h
 * @date		Feb 8 2015
 * @copyright		(C) 2015 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for the pmc API.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "pmc_learn.h"
#include "pmc_api.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern struct edges_collector_t *pmc_api_learn_model(char *data_buf,
						double validperc,
						double cov,
						double maxerrperc,
						const double mincov,
						const size_t ncondmax,
						const size_t ncondmin,
						const char autodim)
{
	struct data_set_t data;
	struct arguments_t arguments;
	size_t *errmaxv;
	struct edges_collector_t *edcoll;

	arguments.silent = 0;
	arguments.probability = 0.9;
	arguments.verbose = 1;

	if (load_data_from_buf(&data, data_buf, 0, 0, 0))
		return NULL;

	set_validation_perc(&data, validperc / 100);
	errmaxv = init_err_max(data.nout, data.nout_class, maxerrperc);

#ifdef OPENCL
	load_chunks_to_device(data.c, data.ncol, data.nout);
#endif

	/*---------------------------------------------------------------------
	 *-- LEARNING MODEL ---------------------------------------------------
	 *---------------------------------------------------------------------
	 */
	edcoll = learn_pmc_model(&data, cov, mincov, arguments, errmaxv,
				ncondmax, ncondmin, autodim);
	/*---------------------------------------------------------------------
	 *-- LEARNING MODEL END -----------------------------------------------
	 *---------------------------------------------------------------------
	 */

#ifdef OPENCL
	free_chunks_on_device(data.ncol, data.nout);
#endif
	free_data_set(&data);
	free(errmaxv);

	return edcoll;
}

extern  size_t *init_err_max(size_t nout, size_t *noutclass, double maxerrperc)
{
	uint32_t i;
	size_t count_other, j;
	size_t *errmaxv;

	errmaxv = malloc(nout * sizeof(*errmaxv));
	for (i = 0; i < nout; i++) {
		count_other = 0;
		for (j = 0; j < nout; j++)
			if (j != i)
				count_other += noutclass[i];
		errmaxv[i] = maxerrperc * count_other / 100;
	}

	return errmaxv;
}
