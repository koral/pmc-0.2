/**
 * @file		pmc-api-test.c
 * @date		Feb 10 2015
 * @copyright		(C) 2015 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Test file for the pmc API.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "pmc_api.h"
#include "pmc_learn.h"

int main(int argc, char **argv)
{
	size_t f_size;
	FILE *f;
	char *data_buf;
	struct edges_collector_t *ed_coll;

	f = fopen("../datasets/2dchess_1000p_1000v_10dim_0err.float", "r");
	if (!f)
		return -1;

	fseek(f, 0, SEEK_END);
	f_size = ftell(f);
	fseek(f, 0, SEEK_SET);
	data_buf = malloc(f_size);
	if (fread(data_buf, f_size, 1, f) != 1)
		return -1;
	fclose(f);

	ed_coll = pmc_api_learn_model(data_buf,
				0,
				10,
				0,
				5,
				10,
				1,
				0);

	return 0;
}
