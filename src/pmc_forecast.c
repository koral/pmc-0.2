/**
 * @file		pmc_forecast.c
 * @date		Aug 31 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for forecast related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include "pmc_forecast.h"

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 * Apply a rule to the data.
 * When a pattern is mached by the rule the rule covering is added in the
 * appropriate chunck for that rule class.
 *
 * @param[in] nout		output classes number
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] ncol		number of variables in the data matrix
 * @param[in,out] chunks	chunks data matrix
 */
static void apply_rule(int32_t nout, struct edges_t *edges, size_t ncol,
		struct data_chunk_t ***chunks);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern void apply_rules(size_t *errtr_ref, size_t *errval_ref, int32_t nout,
			struct edges_collector_t *edcoll, size_t ncol,
			struct data_chunk_t ***chunks)
{
	size_t i, j, k;
	int32_t maxcol;

	for (j = 0; j < nout; j++)
		for (i = ncol; i < ncol + nout + 1; i++)
			memset(chunks[j][i]->v, -1,
				chunks[j][0]->len * sizeof(chunks[j][i]->v[0]));

	for (i = 0; i < edcoll->len; i++)
		apply_rule(nout, edcoll->ed[i], ncol, chunks);

	for (j = 0; j < nout; j++)
		for (k = 0; k < chunks[j][0]->len; k++) {
			maxcol = rand() % nout;
			for (i = ncol; i < ncol + nout; i++)
				if (chunks[j][i]->v[k].i > maxcol)
					maxcol = i - ncol;
			chunks[j][ncol + nout]->v[k].i = maxcol;
			if (chunks[j][ncol + nout]->v[k].i != j) {
				if (k < chunks[j][0]->trainlen)
					(*errtr_ref)++;
				else
					(*errval_ref)++;
			}
		}

	return;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static void apply_rule(int32_t nout, struct edges_t *edges, size_t ncol,
		struct data_chunk_t ***chunks)
{
	register size_t i, j, k, res;
	register int32_t *pend;
	register int32_t **p;

	p = alloca(edges->ndim * sizeof(*p) + 63);
	p = (int32_t **)(((uintptr_t)p + 63)&~(uintptr_t)0x3F);

	for (j = 0; j < nout; j++) {
		for (i = 0; i < edges->ndim; i++)
			p[i] = (int32_t *)chunks[j][edges->comb[i]]->v;
		pend = (int32_t *)chunks[j][edges->comb[0]]->v +
			chunks[j][edges->comb[0]]->len;

		while (*p < pend) {
			res = 0;
			for (i = 0; i < edges->ndim; i++) {
				k = edges->comb[i];
				if (edges->min[k].i <= *p[i] &&
					edges->max[k].i >= *p[i])
					res++;
				p[i]++;
			}
			if (res == edges->ndim)
				chunks[j][ncol + edges->nclass]->
					v[chunks[j][0]->len - (pend - *p) - 1].i
					+= (int32_t)(edges->cov);
		}
	}

	return;
}
