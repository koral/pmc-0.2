/**
 * @file		pmc-preparehdfiles.c
 * @date		Nov 7 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Prepare files to be cached in hadoop
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <argp.h>
#include "pmc_base.h"
#include "pmc_dataset.h"
#include "pmc_ind.h"
#include "pmc_comb.h"

const char *argp_program_version =
	"pmc-preparehdfiles 0.1";
const char *argp_program_bug_address =
	"<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
	"pmc-preparehdfiles take a discretized dataset and prepare the files "
	"for the hadoop run";
/* Description of the arguments we accept. */
static char args_doc[] = "[FILE]";
/* Options we understand. */
static struct argp_option options[] = {
	{"probability",  'p', "NUM",      0,  "The rule detection probability."
	 " Default value: "
	 STR(RULEPROBDECT), 0},
	{"quiet",    'q', 0,      0,  "Don't produce any progress output" , 0},
	{"mincond",    'm', "NUM",      0,  "Minumum number of condition for "
	 "rules. Default value: "
	 STR(MINCONDDEFAULT), 0},
	{"maxcond",    'd', "NUM",      0,  "Maximum number of condition for "
	 "rules generated. Default value is the number of the input "
	 "attributes.", 0},
	{"errormax",    'e', "PERC",      0,  "Maximum error accepted for "
	 "the rules generated expressend in percentage. Default value: "
	 STR(MAXERRDEFAULT), 0},
	{"mincovering",    'c', "PERC",      0,  "Minimum covering accepted "
	 "for rules expressend in percentage. Default value: "
	 STR(MINCOVDEFAULT), 0},
	{ 0 }
};
/* To communicate with parse_opt. */
struct arguments_t {
	char *args[1];                /* arg1  */
	char autodim;
	int silent, ncondmax, ncondmin;
	double mincov, maxerrperc, probability;
	char *outfile;
} ;

/**
 * Parse a single option.
 * Get the input argument from argp_parse, which we
 * know is a pointer to our arguments structure.
 *
 * @param[in] key
 * @param[in] arg
 * @param[out] state
 *
 * @return
 */
static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	struct arguments_t *arguments = state->input;

	switch (key)
	{
	case 'p':
		arguments->probability = 1 - atof(arg);
		break;
	case 'm':
		arguments->ncondmin = atoi(arg);
		break;
	case 'd':
		arguments->ncondmax = atoi(arg);
		break;
	case 'e':
		arguments->maxerrperc = atof(arg);
		break;
	case 'c':
		arguments->mincov = atof(arg);
		break;
	case 'a':
		arguments->autodim = 1;
		break;
	case 'q':
		arguments->silent = 1;
		break;

	case ARGP_KEY_ARG:
		if (state->arg_num >= 2)
			/* Too many arguments. */
			argp_usage (state);

		arguments->args[state->arg_num] = arg;

		break;

	case ARGP_KEY_END:
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}
/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0};


/**
 * Main function for pmc-preparehdfiles executable.
 *
 * @param[in] argc
 * @param[in] argv
 *
 * @return
 */
int main(int argc, char **argv)
{
	double mincov, maxerrperc;
	size_t i, j, nextract, ncondmax, len_comb, chunksize;
	char filename[MAXSTRING];
	struct data_set_t data;
	struct arguments_t arguments;
	FILE *pfile;
	char **cind;
	size_t **combs;


	arguments.probability = 1 - RULEPROBDECT;
	arguments.silent = 0;
	arguments.mincov = MINCOVDEFAULT;
	arguments.maxerrperc = MAXERRDEFAULT;
	arguments.ncondmin = MINCONDDEFAULT;
	arguments.ncondmax = INT32_MAX;
	arguments.autodim = 0;

	/* Parse our arguments; every option seen by parse_opt will
	   be reflected in arguments. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	load_data_from_file(&data, arguments.args[0], 0, !arguments.silent);

	data.ncol--;

	ncondmax = arguments.ncondmax;
	if (ncondmax == INT32_MAX)
		ncondmax = data.ncol;
	mincov = arguments.mincov;
	maxerrperc = arguments.maxerrperc;
	set_validation_perc(&data, 0);

	/* write data chunks */

	for (i = 0; i < data.nout; i++)
		for (j = 0; j < data.ncol; j++) {
			sprintf(filename, STR(TEMPFILEDIR)
				STR(CHUNKFILENAME), (int32_t)i, j);
			pfile = fopen(filename, "wb");
			if (!pfile)
				error();
			chunksize = (data.c[i][j]->len - 1) *
				sizeof(*data.c[i][j]->v) +
				sizeof(struct data_chunk_t);
			fwrite (data.c[i][j] , 1, chunksize, pfile);
			fclose (pfile);
		}

	/* write input file */
	pfile = fopen (STR(TEMPFILEDIR)
		STR(HDINPUTFILENAME), "wb");
	for (nextract = 1; nextract <= ncondmax; nextract++) {
		gen_combs(&combs, &len_comb, data.ncol, nextract);

		for(i = 0; i < len_comb; i++) {
			fprintf(pfile, STR(SIZE_T_P) " ", data.ncol);
			fprintf(pfile, "%d ", data.nout);
			fprintf(pfile, STR(SIZE_T_P) " ", nextract);
			for(j = 0; j < nextract; j++)
				fprintf(pfile, STR(SIZE_T_P) " ", combs[i][j]);
			for(j = 0; j < data.nout; j++)
				fprintf(pfile, STR(SIZE_T_P) " ",
					data.nout_class[j]);
			fprintf(pfile, "%f ", maxerrperc);
			fprintf(pfile, "%f \n", mincov);
		}
		free_combs(combs, len_comb);
	}
	fclose(pfile);

	cind = init_chunks_ind(&data,
			estimate_sample(mincov / 100,
					arguments.probability));

	/* write index files */

	for (i = 0; i < data.nout; i++) {
		sprintf(filename, STR(TEMPFILEDIR)
			STR(CHUNKINDFILENAME), (int32_t)i);
		pfile = fopen(filename, "wb");
		if (!pfile)
			error();
		fwrite(cind[i], 1, data.c[i][0]->len, pfile);
		fclose(pfile);
	}

	free_chunks(data.c, data.ncol, data.nout);
	free(data.nout_class);

	return 0;
}
