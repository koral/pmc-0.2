/**
 * @file		pmc_utils.c
 * @date		Aug 2 2014
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Utilities function for the PMAlgo
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include <math.h>
#include "pmc_utils.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern float find_n_el(float *v, size_t ind)
{
	size_t i = 0;
	size_t j = 0;

	if (!ind)
		return v[0];

	while (i != ind) {
		if (v[j + 1] != v[j])
			i++;
		j++;
	}

	return v[j];
}

extern void char_replace(char *str, char origc, char newc)
{

	while(*(str++) != '\0')
		if (*str == origc)
			*str = newc;

	return;
}

extern int32_t cmp_float(const void *a, const void * b)
{

	if((*(float *)a - *(float *)b) > 0)
		return 1;
	else
		return 0;
}

extern int32_t cmp_int32(const void *a, const void * b)
{

	return (int32_t)(*(int32_t *)a - *(int32_t *)b);
}

extern size_t find_ind_int64(size_t *v, size_t el, size_t maxlen)
{
	size_t i;

	for (i = 0; i < maxlen; i++) {
		if (v[i] == el)
			return i;
	}

	return -1;
}

extern size_t find_ind_float(float *v, float el, size_t maxlen)
{
	size_t i;

	for (i = 0; i < maxlen; i++)
		if (v[i] == el)
			return i;
	return -1;
}

extern char *find_new_el(char *filebuf, char sep)
{
	size_t i;

	for (i = 0; filebuf[i + 1] != '\0'; i++)
		if (filebuf[i] == sep)
			return filebuf + i + 1;
	return NULL;
}

extern char *print_rules(struct edges_t *ed, size_t *comb, char_array_t *names,
			char *types)
{
	size_t i, k;
	char_array_t namesalt;
	enum data_type_t type;
	char tmp[MAXSTRING];
	char *buf;
	char tmpflag = 0;

	if (!names) {
		size_t maxcomb = 0;

		tmpflag = 1;
		names = &namesalt;
		init_char_array(names);
		for (i = 0; i < ed->ndim; i++)
			if (ed->comb[i] > maxcomb)
				maxcomb = ed->comb[i];
		resize_char_array(names, maxcomb + 1);

		for (i = 0; i <= maxcomb; i++) {
			names->c[i] = alloca(MAXSTRING);
			sprintf(tmp, STR(INPUTDEFAULTNAME), i);
			strcpy(names->c[i], tmp);
		}
	}

	buf = calloc(MAXSTRING, sizeof(*buf));

	if(!comb) {
		comb = alloca(ed->ndim * sizeof(*comb));
		for (i = 0; i < ed->ndim; i++)
			comb[i] = i;
	}

	sprintf(tmp, "%.2f%%=Cov\t%.2f%%=Err\t", ed->cov, ed->err);
	strcat(buf, tmp);

	for (i = 0; i < ed->ndim; i++) {
		k = comb[i];
		if (types)
			type = types[k];
		else
			type = 'f';

		if (type == 'i') {
			if (ed->min[k].i == MINVAL && ed->max[k].i == MAXVAL)
				printf(tmp, "-inf<%s<+inf\t",
					names->c[ed->comb[i]]);
			else if (ed->min[k].i == MINVAL)
				sprintf(tmp, "%s<=%d\t", names->c[ed->comb[i]],
					ed->max[k].i);
			else if (ed->max[k].i == MAXVAL)
				sprintf(tmp, "%d<=%s\t", ed->min[k].i,
					names->c[ed->comb[i]]);
			else
				sprintf(tmp, "%d<=%s<=%d\t", ed->min[k].i,
					names->c[ed->comb[i]], ed->max[k].i);
		} else {
			if (ed->min[k].i == MINVAL && ed->max[k].i == MAXVAL)
				printf(tmp, "-inf<%s<+inf\t",
					names->c[ed->comb[i]]);
			else if (ed->min[k].i == MINVAL)
				sprintf(tmp, "%s<=%f\t", names->c[ed->comb[i]],
					dec_f(ed->max[k].e));
			else if (ed->max[k].i == MAXVAL)
				sprintf(tmp, "%f<=%s\t", dec_f(ed->min[k].e),
					names->c[ed->comb[i]]);
			else
				sprintf(tmp, "%f<=%s<=%f\t",
					dec_f(ed->min[k].e),
					names->c[ed->comb[i]],
					dec_f(ed->max[k].e));
		}
		strcat(buf, tmp);
	}
	sprintf(tmp, "OUT=%d\n", ed->nclass);
	strcat(buf, tmp);

	if (tmpflag)
		free(names->c);

	return buf;
}

extern size_t estimate_sample(double cov, double probthresh)
{
	size_t i = 0;
	double prob = 1;

	if (probthresh == 0)
		return -1;
	while (prob > probthresh)
		prob = pow((1 - cov), (double)(++i));

	return i;
}

extern char *read_line()
{
	char *linen;
	char *line = malloc(MAXSTRING), * linep = line;
	size_t lenmax = MAXSTRING, len = lenmax;
	int c;

	if(line == NULL)
		return NULL;

	for(;;) {
		c = fgetc(stdin);
		if(c == EOF)
			break;

		if(--len == 0) {
			linen = realloc(linep, lenmax *= 2);
			len = lenmax;

			if(linen == NULL) {
				free(linep);
				return NULL;
			}
			line = linen + (line - linep);
			linep = linen;
		}

		if((*line++ = c) == '\n')
			break;
	}
	*line = '\0';
	return linep;
}

extern char file_load(char *filename, char **buf_ref, size_t *size)
{
	size_t pos;
	FILE *f;
	char *filebuf = NULL;

	if (strcmp(filename, "-")) {
		f = fopen(filename, "rb");
		if (f) {
			fseek(f, 0, SEEK_END);
			pos = ftell(f);
			fseek(f, 0, SEEK_SET);
			filebuf = malloc(pos + 1);
			fread(filebuf, 1, pos, f);
			fclose(f);
			filebuf[pos] = '\0';
			*size = pos + 1;
			*buf_ref = filebuf;

			return 0;
		} else {
			return 1;
		}
	} else {
		/* Load data from stdin */
		pos = 0;
		*size = MAXSTRING;
		filebuf = malloc(*size);
		while ((filebuf[pos++] = getc(stdin)) != EOF) {
			if (pos == *size) {
				*size *= 2;
				filebuf = realloc(filebuf, *size);
			}
		}
		filebuf[pos - 1] = '\0';
		*size = pos;
		*buf_ref = filebuf;
	}

	return 0;
}

extern void shift_char_p(char **charp_ref)
{
	char *charp;

	charp = *charp_ref;
	charp++;
	while (*charp != ' ' && *charp != '\0')
		charp++;
	*charp_ref = charp;
}

extern void error()
{
	printf("sorry something bad append :\\ \n");

	exit(1);
}
