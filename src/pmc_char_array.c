/**
 * @file		pmc_char_array.c
 * @date		Sept 01 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation for function related to struct
 *			char_array_t.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_char_array.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern void init_char_array(char_array_t *carray)
{
	carray->len = 0;
	carray->maxlen = 0;
	carray->c = NULL;

	return;
}

extern void free_char_array(char_array_t *carray)
{
	size_t i;

	for (i = 0; i < carray->len; i++)
		free(carray->c[i]);
	free(carray->c);
}

extern void resize_char_array(char_array_t *carray, size_t size)
{
	carray->c = realloc((void *)carray->c, size * sizeof(carray->c[0]));
	if (!carray->c)
		error();

	carray->maxlen = size;
	memset(carray->c + carray->len, 0, carray->maxlen - carray->len);

	return;
}

extern size_t add_str_to_char_array(char *str, char_array_t *carray,
				char checkdup)
{

	if (checkdup) {
		size_t ctmp = 0;

		ctmp = find_el_char_array(carray, str);
		if (ctmp != SIZE_MAX)
			return ctmp;
	}

	if (carray->len == carray->maxlen)
		resize_char_array(carray, carray->maxlen + STRINGSTEP);
	carray->c[carray->len] = malloc(MAXSTRING);
	strcpy(carray->c[carray->len], str);

	return carray->len++;
}

size_t find_el_char_array(char_array_t *carray, char *str)
{
	size_t i = 0, count = SIZE_MAX;

	while(i < carray->len) {
		if (!strcmp(str, carray->c[i++])) {
			count = i - 1;
			break;
		}
	}
	if (count != SIZE_MAX)
		return count;

	return SIZE_MAX;
}
