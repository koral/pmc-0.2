/**
 * @file		pmc_dataset.c
 * @date		Aug 28 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for dataset struct and related
 *			functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include "pmc_dataset.h"
#include "pmc_utils.h"

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 * Performs needed allocations in the data_set_t structure.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to free
 *
 * @return			Returns 0 on success or -1 on error.
 */
static int init_data_set(struct data_set_t *data);

/**
 * Given dataset finds the space limits and set the internal fields
 * (uses only the training set).
 *
 * @param[in,out] data
 */
static void set_limits(struct data_set_t *data);

/**
 * Given a text buffer load all the output and return the correspondig
 * char_array_t struct.
 *
 * @param[in] filebuf		input text buffer
 * @param[in] nline		number of lines in the buffer
 * @param[in] outpos		position of the output
 * @param[in] sepc		separator character
 * @param[out] noutclass_ref	array containing the frequency of the different
 *				classes, by reference
 * @param[out] nout_ref		total number of output classes by reference, by
 * 				reference
 * @param[out] charray		char_array_t struct containing the output
 *				classes
 */
static void load_out(char *filebuf, size_t nline, size_t outpos, char sepc,
	size_t **noutclass_ref, uint32_t *nout_ref, char_array_t *charray);

/**
 * Load non homogeneous data from a buffer once that the first line as been
 * parsed.
 * @sa LoadHeterogeneousFile
 *
 * @param[in] ncol		total number of input varialbes
 * @param[in] type		type array
 * @param[in] filebuf		buffer containing the loaded file
 * @param[in] sepc		seaprator char
 * @param[in] ind		index vector
 * @param[in] nline		line number
 * @param[out] names_ref	variable names in char_array_t struct by
 *				reference
 * @param[in] argnames	if true load names for output classes
 * @param[in] argdiscint	obsolete!?
 * @param[in] argmiss		missing value character (by reference!?)
 * @param[in] col		data matrix of elem_32_t
 * @param[out] carray_ref	output classes names char_array_t
 */
static void load_eterogeneous_buf(size_t ncol, char *type, char *filebuf,
				char sepc, size_t *ind, size_t nline,
				char_array_t *names_ref, int argnames,
				int argdiscint, char *argmiss,
				union elem_32_t **col,
				char_array_t *carray_ref);

/**
 * Given a text buffer return the number of column.
 *
 * @param[in] filebuf	input text buffer
 * @param[in] sep	separator character
 *
 * @return		number of column
 */
static size_t count_col(char *filebuf, char sep);

/**
 * Count the number of lines in a file.
 *
 * @param[in] filebuf		buffer in which to perform the search
 *
 * @return			line number
 */
static size_t count_lines(char *filebuf);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern int load_data_from_buf(struct data_set_t *data, char *data_buf,
			size_t buf_size, int shortname, int verbose)
{
	char chtmp;
	size_t i, j, k, outpos;
	char str[MAXSTRING];
	char *data_buf_orig;
	size_t *ind;

	if (buf_size == -1) {
		data_buf_orig = data_buf;
		for ( ; !*data_buf_orig; data_buf_orig++)
			;
		buf_size = data_buf_orig - data_buf;
	}

	init_data_set(data);

	data_buf_orig = data_buf;
	data->ncol = count_col(data_buf, '\t');

	data->types = malloc(data->ncol * sizeof(data->types[0]));
	ind = alloca(data->ncol * sizeof(*ind));

	/* Load input types */

	for (i = 0; i < data->ncol; i++) {
		data->types[i] = data_buf[0];
		if (i != data->ncol - 1)
			data_buf = find_new_el(data_buf, '\t');
		else
			data_buf = find_new_el(data_buf, '\n');
	}
	data->types[data->ncol] = data_buf[0];
	/* data_buf = find_new_el(data_buf, '\n'); */
	j = 0;
	outpos = data->ncol - 1;
	for (i = 0; i < data->ncol + 1; i++) {
		if (data->types[i]=='o') {
			j = 1;
			k = i;
			outpos = i + 1;
		} else {
			ind[i] = i - j;
		}
	}
	ind[outpos] = data->ncol - 1;
	/* nline--; */

	/* Read input names line */
	init_char_array(data->names);
	resize_char_array(data->names, data->ncol);
	for (i = 0; i < data->ncol - 1; i++) {
		for (k = 0; data_buf[k] != '\t' && data_buf[k] != '\n'; k++)
			;
		chtmp = data_buf[k];
		data_buf[k] = '\0';
		strcpy(str, data_buf);
		if (shortname)
			char_replace(str, '#', '\0');
		add_str_to_char_array(str, data->names, 0);
		data_buf[k] = chtmp;
		data_buf += k + 1;
	}
	data_buf = find_new_el(data_buf, '\n');
	data->nelem = count_lines(data_buf);

	load_out(data_buf, data->nelem, outpos, '\t', &data->nout_class, &data->nout, data->out);

	init_chunks(&data->c, data->ncol, data->nout_class, data->nout, data->types);
	fill_chunks(data->c, data_buf, data->ncol, data->nelem, outpos, data->out, ind);

	if (verbose) {
		printf("file has " STR(SIZE_T_P) " lines\n", data->nelem + 1);
		printf("file has " STR(SIZE_T_P) " cols\n", data->ncol + 1);
	}

	free(data_buf_orig);

	data->ncol--;
	data->limits = init_edges(data->ncol);


	return 0;
}

extern int load_data_from_file(struct data_set_t *data, char *filename,
			int shortname, int verbose)
{
	char *filebuf;
	size_t fsize;

	if (file_load(filename, &filebuf, &fsize)) {
		perror("Error opening dataset file");
		return -1;
	}

	load_data_from_buf(data, filebuf, fsize, shortname, verbose);

	return 0;
}

extern int load_data_form_cache(struct data_set_t *data, size_t *comb,
				size_t len_comb, uint32_t nout)
{
	data->c = load_chunks_from_cache(len_comb, comb, nout);
	if (!data->c) {
		error();
		return -1;
	}
	data->ncol = len_comb;
	data->nout = nout;
	data->limits = init_edges(len_comb);
	set_limits(data);

	return 0;
}

extern void set_validation_perc(struct data_set_t *data, double validperc)
{
	size_t i, j;

	for (i = 0; i < data->nout; i++)
		for (j = 0; j < data->ncol + data->nout; j++)
			data->c[i][j]->trainlen =
				data->c[i][j]->len * (1 - validperc);
	set_limits(data);

	return;
}

extern void free_data_set(struct data_set_t *data)
{
	free_char_array(data->names);
	free_char_array(data->out);
	free_chunks(data->c, data->ncol + 1, data->nout);
	free_edges(data->limits);
	free(data->nout_class);
	free(data->names);
	free(data->out);

	return;
}

/* extern void load_heterogeneous_file(char *filename, size_t *ncol_ref, */
/* 				char **type_ref, char sepc, size_t **ind_ref, */
/* 				size_t *nline_ref, char_array_t *names_ref, */
/* 				char *argmiss, union elem_32_t ***col_ref, */
/* 				char_array_t *carray_ref) */
extern void load_heterogeneous_file(char *filename, size_t *ncol_ref,
				char **type_ref, char sepc, size_t **ind_ref,
				size_t *nline_ref, char_array_t *names_ref,
				int argnames, int argdiscint, char *argmiss,
				union elem_32_t ***col_ref,
				char_array_t *carray_ref)
{
	size_t fsize, i;
	char *filebuforig, *filebuf;
	size_t nline = *nline_ref;
	size_t ncol = *ncol_ref;
	char *type = *type_ref;
	size_t *ind = *ind_ref;
	union elem_32_t **col = *col_ref;

	file_load(filename, &filebuf, &fsize);
	filebuforig = filebuf;
	nline = count_lines(filebuf);
	ncol = count_col(filebuf, sepc);
	type = malloc(ncol * sizeof(*type));
	col = malloc(ncol * sizeof(*col));
	ind = malloc(ncol * sizeof(*ind));
	for (i = 0; i < ncol; i++)
		col[i] = malloc((nline - 1) * sizeof(**col));

	load_eterogeneous_buf(ncol, type, filebuf, sepc, ind, nline, names_ref,
			argnames, argdiscint, argmiss, col, carray_ref);

	*nline_ref = nline;
	*ncol_ref = ncol;
	*col_ref = col;
	*ind_ref = ind;
	*type_ref = type;

	free(filebuforig);

	return;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static int init_data_set(struct data_set_t *data)
{
	data->names = malloc(sizeof(*data->names));
	data->out = malloc(sizeof(*data->out));

	if (!data->names || !data->out) {
		error();
		return -1;
	}

	return 0;
}

static void set_limits(struct data_set_t *data)
{
	int32_t **min, **max;
	size_t i, j;

	max = alloca(data->nout * sizeof(*max));
	min = alloca(data->nout * sizeof(*min));
	for (i=0; i<data->nout; i++) {
		max[i] = alloca(data->ncol * sizeof(**max));
		min[i] = alloca(data->ncol * sizeof(**min));
	}
	for (i = 0; i < data->nout; i++)
		for (j = 0; j < data->ncol; j++)
			find_chunk_max_min(
				data->c[i][j], &max[i][j], &min[i][j]);

	for (j = 0; j < data->ncol; j++) {
		data->limits->max[j].i = max[0][j];
		data->limits->min[j].i = min[0][j];
		for (i = 1; i < data->nout; i++) {
			if (data->limits->max[j].i < max[i][j])
				data->limits->max[j].i = max[i][j];
			if (data->limits->min[j].i > min[i][j])
				data->limits->min[j].i = min[i][j];
		}
	}

	return;
}

static void load_out(char *filebuf, size_t nline, size_t outpos, char sepc,
	size_t **noutclass_ref, uint32_t *nout_ref, char_array_t *charray)
{
	char chtmp;
	int32_t i, j;
	size_t *noutclass;

	noutclass = calloc(sizeof(*noutclass), MAXCLASSOUT);

	init_char_array(charray);

	for (i = 0; i < nline; i++) {
		if (outpos)
			for (j = 0; j < outpos - 1; j++)
				filebuf = find_new_el(filebuf, sepc);
		for (j = 0; filebuf[j] != sepc && filebuf[j] != '\n'; j++)
			;
		chtmp = filebuf[j];
		filebuf[j] = '\0';
		noutclass[add_str_to_char_array(filebuf, charray, 1)]++;
		filebuf[j] = chtmp;
		if (i != nline)
			filebuf = find_new_el(filebuf, '\n');
	}

	*nout_ref = charray->len;
	*noutclass_ref = noutclass;

	return;
}

static void load_eterogeneous_buf(size_t ncol, char *type, char *filebuf,
				char sepc, size_t *ind, size_t nline,
				char_array_t *names_ref, int argnames,
				int argdiscint, char *argmiss,
				union elem_32_t **col,
				char_array_t *carray_ref)
{
	size_t i, j, k, ctmp, outpos;
	char chtmp;

	/* Load input types */

	for (i = 0; i < ncol - 1; i++) {
		type[i] = filebuf[0];
		filebuf = find_new_el(filebuf, sepc);
	}
	type[ncol - 1] = filebuf[0];
	filebuf = find_new_el(filebuf, '\n');
	j = 0;
	outpos = ncol - 1;
	for (i = 0; i < ncol; i++) {
		if (type[i] == 'o') {
			j = 1;
			k = i;
			outpos = i;
		} else {
			ind[i] = i - j;
		}
	}
	ind[outpos] = ncol - 1;
	nline--;

	/* Load input names if requested */

	init_char_array(names_ref);
	resize_char_array(names_ref, ncol);
	if (argnames) {
		char outname[MAXSTRING];

		for (i = 0; i < ncol; i++) {
			for (k = 0; filebuf[k] != sepc && filebuf[k] != '\n';
			     k++)
				;
			chtmp = filebuf[k];
			filebuf[k] = '\0';
			if (type[i] == 'o') {
				sprintf(outname, "%s#" STR(SIZE_T_P),
					filebuf, i);
			} else {
				char str[MAXSTRING];

				sprintf(str, "%s#" STR(SIZE_T_P),
					filebuf, i);
				if (type[i] == 'i') {
					strcat(str, "#i");
					if (argdiscint)
						strcat(str, "d");
				} else {
					if (type[i]=='f')
						strcat(str, "#f");
				}
				add_str_to_char_array(str, names_ref, 0);
			}
			filebuf[k] = chtmp;
			if (i != ncol - 1)
				filebuf = find_new_el(filebuf, sepc);
		}
		/* strcat(outname, "#o"); */
		add_str_to_char_array(outname, names_ref, 0);
		filebuf = find_new_el(filebuf, '\n');
		nline--;
	} else {
		char str[MAXSTRING];

		for (i = 0; i < ncol; i++) {
			if (type[i] != 'o') {
				sprintf(str, STR(INPUTDEFAULTNAME), ind[i]);
				if (type[i] == 'i') {
					strcat(str, "#i");
					if (argdiscint)
						strcat(str, "d");
				} else if (type[i] == 'f')
					strcat(str, "#f");
				add_str_to_char_array(str, names_ref, 0);
			}
		}
		sprintf(str, "OUT#" STR(SIZE_T_P), outpos);
		add_str_to_char_array(str, names_ref, 0);
	}


	for (i = 0; i < nline; i++)
		for (j = 0; j < ncol; j++) {
			for (k = 0; filebuf[k] != sepc && filebuf[k] != '\n';
			     k++)
				;
			chtmp = filebuf[k];
			filebuf[k] = '\0';
			if (strcmp(filebuf, argmiss))
				switch (type[j]) {
				case 'i':
					col[ind[j]][i].i = atoi(filebuf);
					break;
				case 'f':
					col[ind[j]][i].e = enc_f(atof(filebuf));
					break;
				case 'o':
					if (i < nline - 1) {
						ctmp = add_str_to_char_array(
							filebuf, carray_ref, 1);
						col[ncol - 1][i].i = ctmp;
					} else {
						ctmp = add_str_to_char_array(
							filebuf, carray_ref, 1);
						col[ncol - 1][i].i = ctmp;
					}
					break;
				default :
					fprintf(stderr, "Invalid type for "
						"column " STR(SIZE_T_P)
						"...\n", j);
					exit(1);
				} else {
				col[ind[j]][i].i = MISSVAL;
			}

			filebuf[k] = chtmp;
			if (j != ncol - 1)
				filebuf = find_new_el(filebuf, sepc);
			else
				filebuf = find_new_el(filebuf, '\n');
		}

	return;
}

static size_t count_col(char *filebuf, char sep)
{
	size_t i;
	size_t nsep = 0;

	for (i = 0; filebuf[i + 1] != '\n'; i++)
		if (filebuf[i] == sep)
			nsep++;
	return ++nsep;
}

static size_t count_lines(char *filebuf)
{
	size_t i;
	size_t lines = 0;

	for (i = 0; filebuf[i + 1]!='\0'; i++)
		if (filebuf[i]=='\n')
			lines++;
	return ++lines;
}
