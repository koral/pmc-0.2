/**
 * @file		pmc_forecast.c
 * @date		Aug 31 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for forecast related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_forecast_h_included_
#define _pmc_forecast_h_included_

#include "pmc_chunks.h"

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Apply an entire set of rules
 *
 * @param[out] errtr_ref	training set error by reference
 * @param[out] errval_ref	validation set error by reference
 * @param[in] nout		output classes number
 * @param[in] edcoll		struct edges_collector_t containing the rules to
 *				apply
 * @param[in] ncol		number of variables in the data matrix
 * @param[in] chunks		chunks data matrix
 */
void apply_rules(size_t *errtr_ref, size_t *errval_ref, int32_t nout,
			 struct edges_collector_t *edcoll, size_t ncol,
			 struct data_chunk_t ***chunks);


#endif
