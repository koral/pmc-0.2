/**
 * @file		pmc_datatype.h
 * @date		Sept 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for PMC datatypes.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_datatype_h_included_
#define _pmc_datatype_h_included_

/* This is just to define a 32 type that should not be directly accessed. */
typedef struct {
	int32_t __forbidden;
} encoded_float_t;

/** Enum containing the type of data storable. */
enum data_type_t {INT, FLOAT, ENCODED_FLOAT};

/** Union defining the basic data element storable. */
union elem_32_t {
	/** Integer data. */
	int32_t i;
	/** Float data. */
	float f;
	/** Encoded float data. */
	encoded_float_t e;
};

/**
 * Decode a floating point number.
 * http://kkoral.blogspot.nl/2014/09/agnostic-type-int-float-greatersmaller.html
 *
 * @param[in] f		floating point to decode.
 *
 * @return 		floating point decoded.
 */
static __inline__ float dec_f(encoded_float_t f)
{
	union elem_32_t tmp;
	tmp.i = (*(int32_t *)&f < 0 ? 0x80000000 |
		~*(int32_t *)&f : *(int32_t *)&f);

	return  tmp.f;
}

/**
 * Encode a floating point number.
 * http://kkoral.blogspot.nl/2014/09/agnostic-type-int-float-greatersmaller.html
 *
 * @param[in] f		floating point to encode.
 *
 * @return 		floating point encoded.
 */
static __inline__ encoded_float_t enc_f(float f)
{
	int32_t tmp;
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
	tmp = (f < 0 ? 0x80000000 |
		~*(int32_t *)&f : *(int32_t *)&f);

	return *(encoded_float_t *)&tmp;
#pragma GCC diagnostic pop
}

#endif
