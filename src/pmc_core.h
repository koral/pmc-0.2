/**
 * @file		pmc_core.h
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for PMC algorithm core functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_core_h_included_
#define _pmc_core_h_included_

#include "pmc_ind.h"
#include "pmc_cov.h"

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Given a combination of variables extract the rules
 *
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] comb		Vector with the variable indexes used by the
 *				edge.
 * @param[in] mincov		Minimum covering for accepting a rule.
 * @param[in,out] edcoll_ref	Struct edges_collector_t by reference in which
 *				storing rules.
 * @param[in] len_comb		Number of variable in the combo.
 * @param[in] errmaxv		Maximum error to accept a rule.
 * @param[in] cindext		External char index data matrix.
 *
 * @return			The number of seed used to generate the rules.
 */
extern size_t find_rules(const struct data_set_t *data,
				 size_t *comb,
				 double mincov,
				 struct edges_collector_t **edcoll_ref,
				 size_t len_comb,
				 size_t *errmaxv,
				 char **cindext);

#endif
