/**
 * @file		pmc_cl.c
 * @date		Oct 3 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Core functions for PMC algorithm ported in OpenCL.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_base.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern void init_cl()
{
	size_t i;
	cl_int status;
	char pmcname[MAXSTRING], snum[MAXSTRING];
	char pmcbasename[] = "VChunk";
	char pmcaddbasename[] = "VChunkAdd";
	char clflag[] = "";

	create_context_on(CHOOSE_INTERACTIVELY, CHOOSE_INTERACTIVELY, 0, &ctx,
			&queue, 0);

	print_device_info_from_queue(queue);

	CALL_CL_GUARDED(clGetCommandQueueInfo,
			(queue, CL_QUEUE_DEVICE, sizeof(device), &device,
				NULL));

	CALL_CL_GUARDED(clGetDeviceInfo, (
				device,
				CL_DEVICE_MAX_COMPUTE_UNITS,
				sizeof(cl_uint),
				&computeunits,
				NULL));
	/* find device type */
	status = clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(devtype),
				&devtype, NULL);
	if(status != CL_SUCCESS)
		printf("Unable to get TYPE: %s!\n", cl_error_to_str(status));

	/* load kernels */
	knl_text = read_file("pmc_kernel.cl");
	knl_ReduceTwoStep = kernel_from_string(ctx, knl_text, "ReduceTwoStep",
					clflag);
	knl_VChunk = malloc((MAXCONDCL + 1) * sizeof(*knl_VChunk));
	knl_VChunkAdd = malloc((MAXCONDCL + 1) * sizeof(*knl_VChunkAdd));
	for (i = 1; i <= MAXCONDCL; i++) {
		sprintf(snum, STR(SIZE_T_P), i);
		strcpy(pmcname, pmcbasename);
		knl_VChunk[i] =
			kernel_from_string(ctx, knl_text, strcat(pmcname, snum),
					clflag);
		strcpy(pmcname, pmcaddbasename);
		knl_VChunkAdd[i] = kernel_from_string(ctx, knl_text,
						strcat(pmcname, snum), clflag);
	}

	free(knl_text);

	return;
}

extern char load_chunks_to_device(struct data_chunk_t ***chunks, size_t ncol,
			int32_t nout)
{
	size_t i, j;
	cl_int status;
	size_t maxlen = 0;

	clchunks = malloc(nout * sizeof(*clchunks));

	for (i = 0; i < nout; i++)
		if (maxlen < chunks[i][0]->trainlen)
			maxlen = chunks[i][0]->trainlen;

	for (i = 0; i < nout; i++)
		clchunks[i] = malloc(ncol * sizeof(**clchunks));


	for (i = 0; i < nout; i++)
		for (j = 0; j < ncol; j++) {
			/* create buffers */
			clchunks[i][j] =
				clCreateBuffer(ctx, CL_MEM_READ_ONLY,
					chunks[i][j]->trainlen *
					sizeof(***clchunks), 0, &status);
			CHECK_CL_ERROR(status, "clCreateBuffer");
			/* and fill it */
			CALL_CL_GUARDED(clEnqueueWriteBuffer, (
						queue, clchunks[i][j], CL_TRUE,
						0, chunks[i][j]->trainlen *
						sizeof(***clchunks),
						chunks[i][j]->v, 0, NULL,
						NULL));
		}

	cled = clCreateBuffer(ctx, CL_MEM_READ_ONLY,
			2 * MAXCONDCL * sizeof(*cled), 0, &status);
	CHECK_CL_ERROR(status, "clCreateBuffer");
	cllen = clCreateBuffer(ctx, CL_MEM_READ_ONLY,
			sizeof(cl_uint), 0, &status);
	CHECK_CL_ERROR(status, "clCreateBuffer");
	clvres = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
				maxlen * sizeof(cl_char), 0, &status);
	CHECK_CL_ERROR(status, "clCreateBuffer");
	clvind = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
				maxlen * sizeof(cl_char), 0, &status);
	CHECK_CL_ERROR(status, "clCreateBuffer");

	clvsum = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
				maxlen * sizeof(cl_uint) / 2, 0, &status);
	CHECK_CL_ERROR(status, "clCreateBuffer");

	return 0;
}

extern char free_chunks_on_device(size_t ncol, int32_t nout)
{
	size_t i, j;

	for (i = 0; i < nout; i++) {
		for (j = 0; j < ncol; j++)
			CALL_CL_GUARDED(clReleaseMemObject, (clchunks[i][j]));
		free(clchunks[i]);
	}

	CALL_CL_GUARDED(clReleaseMemObject, (cled));
	CALL_CL_GUARDED(clReleaseMemObject, (cllen));
	CALL_CL_GUARDED(clReleaseMemObject, (clvres));
	CALL_CL_GUARDED(clReleaseMemObject, (clvsum));

	free(clchunks);
	free(knl_VChunk);
	free(knl_VChunkAdd);

	return 0;
}

extern size_t covered_cl_fast(int32_t nout, int32_t nouttot,
			struct edges_t *edges, size_t *inds, size_t ninds,
			size_t countmax, struct data_chunk_t ***chunks)
{
	size_t count;

	if (chunks[nout][0]->trainlen > CLFASTTHRESHOLD && !growing) {
		fastcl = 1;
		count = covered_cl(nout, nouttot, edges, inds, ninds, chunks);
		fastcl = 0;
		if (count > countmax) {
			return count;
		} else {
			count = covered_cl(nout, nouttot, edges, inds, ninds,
					chunks);
			if (count > countmax)
				return count;
			else
				return 0;
		}
	}
	fastcl = 0;
	count = covered_cl(nout, nouttot, edges, inds, ninds, chunks);
	if (count > countmax)
		return count;
	else
		return 0;
}

extern size_t covered_cl(int32_t nout, int32_t nouttot, struct edges_t *edges,
		size_t *inds, size_t ninds, struct data_chunk_t ***chunks)
{
	cl_uint len32;
	size_t len, i, nres;
	/* char *vres;, *cpt, *cptend; */
	cl_uint *vsum;
	size_t count = 0;
	/* struct timeval t0, t1; */
	/* gettimeofday(&t0, 0); */

	len = chunks[nout][0]->trainlen;
	/* if (fastcl) */
	/*   len /= 10; */

	if(len != oldlen) {
		len32 = len;
		CALL_CL_GUARDED(clEnqueueWriteBuffer, (
					queue, cllen, CL_TRUE, 0,
					sizeof(cl_uint), &len32,
					0, NULL, NULL));
		oldlen = len;
	}

	/* ptmpi32 = (int32_t *)alloca(2 * ninds * sizeof(int32_t)); */
	/* for (i=0; i<ninds; i++) { */
	/*   k = inds[i]; */
	/*   ptmpi32[i] = edges->min[k].i; */
	/*   ptmpi32[i + ninds] = edges->max[k].i; */
	/* } */

	/* CALL_CL_GUARDED(clEnqueueWriteBuffer, ( */
	/*		   queue, cled, CL_TRUE, 0, */
	/*		   2 * ninds * sizeof(int32_t), ptmpi32, */
	/*		   0, NULL, NULL)); */

	if (devtype & CL_DEVICE_TYPE_CPU) {
		ldim[0] = computeunits;
		gdim[0] = ((len + ldim[0] - 1) / ldim[0]) * ldim[0];
	} else {
		ldim[0] = WAVEFRONT;
		gdim[0] = ((len + ldim[0] - 1) / ldim[0]) * ldim[0];
	}

	apply_rule_cl(clvres, nout, edges, inds, ninds, 0);

	/* reduce to count covering */
	CALL_CL_GUARDED(clSetKernelArg,
			(knl_ReduceTwoStep, 0, sizeof(clvres), &clvres));
	CALL_CL_GUARDED(clSetKernelArg,
			(knl_ReduceTwoStep, 1, sizeof(ldim[0] * sizeof(char)),
				(void *)NULL));
	CALL_CL_GUARDED(clSetKernelArg,
			(knl_ReduceTwoStep, 2, sizeof(cllen), &cllen));
	CALL_CL_GUARDED(clSetKernelArg,
			(knl_ReduceTwoStep, 3, sizeof(clvsum), &clvsum));

	gdim[0]--;
	gdim[0] |= gdim[0] >> 1;
	gdim[0] |= gdim[0] >> 2;
	gdim[0] |= gdim[0] >> 4;
	gdim[0] |= gdim[0] >> 8;
	gdim[0] |= gdim[0] >> 16;
	gdim[0] |= gdim[0] >> 32;
	gdim[0]++;

	/* if (!(devtype & CL_DEVICE_TYPE_CPU)) { */
	/*   ldim[0] = WAVEFRONTREDUCE; */
	/*   gdim[0] = ((len + ldim[0]-1)/ldim[0])*ldim[0]; */
	/* } */

	CALL_CL_GUARDED(clEnqueueNDRangeKernel,
			(queue, knl_ReduceTwoStep,
				/*dimensions*/ 1, NULL, gdim, ldim,
				0, NULL, NULL));
	nres = len / ldim[0];
	if (len % ldim[0])
		nres++;
	/* take datas home */
	vsum = malloc(nres * sizeof(cl_uint));
	/* CALL_CL_GUARDED(clEnqueueReadBuffer, ( */
	/*		   queue, clvres, CL_TRUE, 0, */
	/*		   len * sizeof(char), vres, */
	/*		   0, NULL, NULL)); */

	CALL_CL_GUARDED(clEnqueueReadBuffer, (
				queue, clvsum, CL_TRUE, 0,
				nres * sizeof(cl_uint), vsum,
				0, NULL, NULL));

	/* count */

	for (i = 1; i < nres - 1; ++i)
		vsum[0] += vsum[i];

	/* cpt = vres; */
	/* cpt--; */
	/* cptend = cpt + len; */

	/* while(++cpt!=cptend) */
	/*   count += *cpt; */

	/* if (count!=vsum[0]) */
	/*   printf("fuck!\n"); */
	count = vsum[0];

	/* free(vres); */
	free(vsum);
	/* gettimeofday(&t1, 0); */
	/* long elapsed =  */
	/* 	(t1.tv_sec - t0.tv_sec)*1000000 + t1.tv_usec - t0.tv_usec; */

	/* printf(STR(SIZE_T_P) "\n", elapsed); */

	return count;
}

extern void apply_rule_cl(cl_mem clv, int32_t nout, struct edges_t *edges,
			size_t *inds, size_t ninds, char additive)
{
	size_t i, k;
	cl_kernel kernel;
	int32_t *ptmpi32;

	if(additive)
		kernel = knl_VChunkAdd[ninds];
	else
		kernel = knl_VChunk[ninds];

	ptmpi32 = alloca(2 * ninds * sizeof(*ptmpi32));
	for (i = 0; i < ninds; i++) {
		k = inds[i];
		ptmpi32[i] = edges->min[k].i;
		ptmpi32[i + ninds] = edges->max[k].i;
	}

	CALL_CL_GUARDED(clEnqueueWriteBuffer, (
				queue, cled, CL_TRUE, 0,
				2 * ninds * sizeof(int32_t), ptmpi32,
				0, NULL, NULL));

	/* set arguments fo the kernel */

	for (i = 0; i < ninds; i++)
		CALL_CL_GUARDED(clSetKernelArg,
				(kernel, i,
					sizeof(clchunks[nout][inds[i]]),
					&(clchunks[nout][inds[i]])));
	CALL_CL_GUARDED(clSetKernelArg,
			(kernel, ninds,
				sizeof(clv),
				&clv));
	CALL_CL_GUARDED(clSetKernelArg,
			(kernel, ninds + 1,
				sizeof(cled),
				&cled));
	CALL_CL_GUARDED(clSetKernelArg,
			(kernel, ninds + 2,
				sizeof(cllen),
				&cllen));

	/* apply rule to the set by kernel */
	CALL_CL_GUARDED(clEnqueueNDRangeKernel,
			(queue, kernel,
				/*dimensions*/ 1, NULL, gdim, ldim,
				0, NULL, NULL));

	return;
}

extern void drop_points_cl(char **cind, int32_t nout, struct edges_t *edges,
			size_t *inds, size_t ninds, struct data_chunk_t ***chunks)
{

	CALL_CL_GUARDED(clEnqueueWriteBuffer, (
				queue, clvind, CL_TRUE, 0,
				chunks[nout][0]->trainlen * sizeof(cl_char),
				cind[nout], 0, NULL, NULL));

	apply_rule_cl(clvind, nout, edges, inds, ninds, 1);

	CALL_CL_GUARDED(clEnqueueReadBuffer, (
				queue, clvind, CL_TRUE, 0,
				chunks[nout][0]->trainlen * sizeof(cl_char),
				cind[nout], 0, NULL, NULL));

	return;
}
