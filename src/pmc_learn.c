/**
 * @file		pmc_learn.c
 * @date		Aug 29 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *			David Vesely <veselda7@gmail.com>
 *
 * @brief		Implementation file for the pmc external learning loop
 *			functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_learn.h"
#include "pmc_comb.h"

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 *
 *
 * @param[in] data		Pointer to the struct data_set_t to use.
 * @param[in] arguments		arguments_t struct carrying arguments.
 * @param[in,out] cindext	External index matrix.
 * @param[in] nseed		Number of seeds to use.
 * @param[in] cov		Actual min covering acceptance.
 * @param[in] nextract		Actual number of feature to used for rule
 *				extraction.
 * @param[in,out] stepcount	Array counting number of step used to explain
 *				the n sudimentional space.
 * @param[in,out] errmaxv	Array containing the max number of pattern
 *				macchable as error for every class.
 * @param[in] edcoll_ref	struct edges_collector_t storing the set of
 *				rules passed by reference.
 */
static void iter_all_combs(const struct data_set_t *data,
			const struct arguments_t arguments,
			char **cindext,
			const size_t nseed,
			const double cov,
			const size_t nextract,
			size_t *stepcount,
			size_t *errmaxv,
			struct edges_collector_t **edcoll_ref);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern struct edges_collector_t *learn_pmc_model(const struct data_set_t *data,
						double cov,
						const double mincov,
						const struct arguments_t
						arguments,
						size_t *errmaxv,
						const size_t ncondmax,
						const size_t ncondmin,
						const char autodim)
{
	size_t nextract, nseed;
	size_t *stepcount;
	char **cindext, **cindaux;
	struct edges_collector_t *edcoll = NULL;

	edcoll = resize_edges_collector(NULL, EDSTEP);
	stepcount = calloc(ncondmax, sizeof(*stepcount));

	cindext = init_chunks_ind(data, -1);
	cindaux = init_chunks_ind(data, -1);

	/* Main loop. */
	/* Iterate collecting rules dividing by two every loop the */
	/* acceptance covering. */
	/* Loop till every seed is explained or mincov is reached. */
	while (cov >= mincov) {
		if (!arguments.silent)
			printf("Extracting rules with covering greater than"
				" %.2f%%\n", cov);
		nseed = estimate_sample(cov / 100,
					arguments.probability);
		set_chunks_ind(cindext, data, nseed);
		ind1_eq_ind2_and_ind2(cindext, cindaux, data);
		nseed = count_ind(cindext, data);
		if (!nseed)
			break;
		if (arguments.verbose)
			printf("Number of seeds: "
				STR(SIZE_T_P) "\n",
				count_ind(cindext, data));

		/* iterate extracting rules incrementing the number */
		/* of variables considered at the same time */
		for (nextract = ncondmin; nextract <= ncondmax; nextract++) {

			if (!arguments.silent)
				printf("Extracting rules with " STR(SIZE_T_P)
					" conds\n",
					nextract);

			/* iterate on all the combination of variables  */
			iter_all_combs(data, arguments, cindext, nseed, cov,
				nextract, stepcount, errmaxv, &edcoll);
			if (!arguments.silent) {
				printf("Progress: 100.00%%\t");
				printf("Explained pattern pergentage: %.2f%%\r",
					100 - 100 * (float) count_ind(
						cindext, data)
					/ nseed);
				fflush(stdout);
				printf("\n");
			}

			/* If autodim is selected do some heuristic to  */
			/* understand if the job is done enough. Obsolete? */
			if (autodim &&
				((nextract > 1 &&
					(float) stepcount[nextract - 1] /
					data->nelem < THRESHOLD) ||
					(nextract > 2 && stepcount[nextract - 2]
						< stepcount[nextract - 1]))) {
				if (!arguments.silent)
					printf("Enough dim analysed going to"
						" sleep...\n");
				break;
			}
		}
		/* Drops the points in cindaux using all the rules extracted */
		drop_points_all_rules(cindaux, edcoll, data);
		cov /= 2;
		nseed = estimate_sample(cov / 100,
					arguments.probability);
	}

	free_chunks_ind(cindaux, data);
	free_chunks_ind(cindext, data);
	free(stepcount);

	return edcoll;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static void iter_all_combs(const struct data_set_t *data,
			const struct arguments_t arguments,
			char **cindext,
			const size_t nseed,
			const double cov,
			const size_t nextract,
			size_t *stepcount,
			size_t *errmaxv,
			struct edges_collector_t **edcoll_ref)
{
	size_t j, tmpc, len_comb;
	size_t **combs;

	gen_combs(&combs, &len_comb, data->ncol, nextract);
	/* iterate on all the combination of variables  */
	for (j = 0; j < len_comb; j++) {
		if (!arguments.silent) {
			printf("Progress: %.2f%%\t", 100 * (float) j / len_comb);
			printf("Explained pattern pergentage: %.2f%%\r",
				100 - 100 *
				(float)count_ind(cindext, data)
				/ nseed);
			fflush(stdout);
		}
		tmpc = find_rules(data, combs[j], cov, edcoll_ref, nextract,
				 errmaxv, cindext);
		if (j == 0)
			stepcount[nextract - 1] = tmpc;
		else
			if (tmpc < stepcount[nextract - 1])
				stepcount[nextract - 1] = tmpc;
	}

	free_combs(combs, len_comb);
}

extern void fprint_rules(FILE *out_stream, int silent, size_t nout,
			struct edges_collector_t *edcoll,  char_array_t names,
			char *types)
{
	uint32_t i;
	size_t j;
	struct rules_t *rules;

	rules = init_rules(nout);

	for (i = 0; i < edcoll->len; i++) {
		char *buf;
		struct edges_t *ed;

		ed = edcoll->ed[i];
		buf = print_rules(ed, ed->comb, &names, types);
		append_rule(buf, &rules[ed->nclass]);
	}

	for (i = 0; i < nout; i++) {
		if (!silent)
			fprintf(out_stream,
				"Printing rules for output number: %d\n", i);
		for (j = 0; j < rules[i].len; j++)
			fprintf(out_stream, "%s", rules[i].v[j]);
	}

	free_rules(rules, nout);

	return;
}

extern void fprintf_results(FILE* out_stream, int silent, double validperc,
			size_t nelem, size_t errtr, size_t errval)
{

	validperc /= 100;
	nelem++;
	if (!silent) {
		fprintf(out_stream,
			"Correctly classified in trainingset : %.2f%% ("
			STR(SIZE_T_P) ")\n",
			100 * (1 - (float) errtr / (nelem * (1 - validperc))),
			(size_t) (nelem * (1 - validperc)) - errtr);
		if (validperc)
			fprintf(out_stream,
				"Correctly classified in validationset : %.2f%%"
				" (" STR(SIZE_T_P) ")\n",
				100 * (1 - (float) errval / (nelem * validperc))
				, (size_t) (nelem * validperc) - errval);
	}
}
