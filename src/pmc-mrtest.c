/**
 * @file		pmc-mrtest.c
 * @date		Nov 8 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Map Reduce algorithm test
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "pmc_base.h"

ssize_t getline(char **lineptr, size_t *n, FILE *stream);

/**
 * Main function for pmc-mrtest.
 *
 * @return
 */
int main()
{
	ssize_t read;
	FILE *pfile;
	char *line = NULL;
	size_t len = 0;

	pfile = fopen(STR(TEMPFILEDIR)
		"input.txt", "r");
	if (pfile == NULL)
		exit(1);
	while ((read = getline(&line, &len, pfile)) != -1) {
		pmc_map(line);
	}
	if (line)
		free(line);
	exit(0);
}
