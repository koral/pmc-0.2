/**
 * @file		pmc_ind.c
 * @date		Aug 29 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for function handling indexes.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_ind.h"

int posix_memalign(void **memptr, size_t alignment, size_t size);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern char **init_chunks_ind(const struct data_set_t *data,
			size_t nsample)
{
	int32_t i;
	char **ind;

	ind = malloc(data->nout * sizeof(*ind));
	if (!ind)
		error();
	for (i = 0; i < data->nout; i++)
		if (posix_memalign((void **)&ind[i], ALIGNSIZE,
					data->c[i][0]->trainlen * sizeof(**ind)))
			error();
	set_chunks_ind(ind, data, nsample);

	return ind;
}

extern void set_chunks_ind(char **ind, const struct data_set_t *data,
			size_t nsample)
{
	int32_t i, seedfract;
	size_t j;

	for (i = 0; i < data->nout; i++) {
		if (nsample == -1) {
			memset((void *)ind[i], 1,
				data->c[i][0]->trainlen * sizeof(**ind));
		} else {
			seedfract = data->c[i][0]->trainlen / nsample;
			if (!seedfract)
				seedfract = 1;
			memset((void *)ind[i], 0,
				data->c[i][0]->trainlen * sizeof(**ind));
			for (j = 0; j < data->c[i][0]->trainlen / seedfract; j++)
			{
				ind[i][j * seedfract] = 1;
			}
		}
	}

	return;
}

extern void ind1_eq_ind2_and_ind2(char** ind1, char** ind2,
				const struct data_set_t *data)
{
	size_t j;
	size_t i = 0;

	for (i = 0; i < data->nout; i++) {
		j = data->c[i][0]->trainlen;
		while (j--)
			ind1[i][j] = ind1[i][j] && ind2[i][j];
	}

	return;
}

extern size_t count_ind(char** ind, const struct data_set_t *data)
{
	size_t j;
	size_t i = 0, sum = 0;

	for (i = 0; i < data->nout; i++) {
		j = data->c[i][0]->trainlen;
		while (j--)
			sum += ind[i][j];
	}

	return sum;
}

extern void free_chunks_ind(char **cind, const struct data_set_t *data)
{
	int32_t i;

	for (i = 0; i < data->nout; i++)
		free(cind[i]);
	free(cind);

	return;
}

extern void drop_points_all_rules(char **cind,
				struct edges_collector_t *edcoll,
				const struct data_set_t *data)
{
	size_t i, j;

	for (i = 0; i < data->nout; i++)
		for (j = 0; j < edcoll->len; j++)
			DROPPOINTS(cind, i, edcoll->ed[j], edcoll->ed[j]->comb,
				edcoll->ed[j]->ndim, data);

	return;
}

extern size_t find_first_seed(char **cind1, char **cind2, int32_t curr_out,
			size_t *comb, size_t len_comb,
			const struct data_set_t *data)
{
	size_t i, j, count;

	for (i = 0; i < data->c[curr_out][0]->trainlen; i++)
		if (cind1[curr_out][i] && cind2[curr_out][i]) {
			count = 0;
			for (j = 0; j < len_comb; j++)
				if (data->c[curr_out][comb[j]]->v[i].i !=
					MISSVAL)
					count++;
			if (count == len_comb)
				return i;
		}

	return -1;
}

extern void drop_points(char **cind, int32_t curr_out, struct edges_t *edges,
			size_t *inds, size_t ninds,
			const struct data_set_t *data)
{
	size_t i, j, k, res;

	for (j = 0; j < data->c[curr_out][0]->trainlen; j++) {
		if (cind[curr_out][j]) {
			res = 0;
			for (i = 0; i < ninds; i++) {
				k = inds[i];
				if (edges->min[k].i <=
					data->c[curr_out][k]->v[j].i &&
					edges->max[k].i >=
					data->c[curr_out][k]->v[j].i)
					res++;
			}
			if (res == ninds)
				cind[curr_out][j] = 0;
		}
	}

	return;
}

extern char **load_ind_from_cache(const struct data_set_t *data)
{
	int32_t i;
	char filename[MAXSTRING];
	FILE *pfile;
	char **cind;

	cind = init_chunks_ind(data, -1);

	for (i = 0; i < data->nout; i++) {
		sprintf(filename, STR(TEMPFILEDIR)
			STR(CHUNKINDFILENAME), i);

		pfile = fopen(filename, "r");
		if (fread(cind[i], data->c[i][0]->len, 1, pfile) != 1)
			error();

		fclose(pfile);
	}

	return cind;
}
