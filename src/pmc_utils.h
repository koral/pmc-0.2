/**
 * @file		pmc_utils.h
 * @date		Aug 2 2014
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for utilities function needed by PMAlgo.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_utils_h_included_
#define _pmc_utils_h_included_

#include "pmc_edges.h"
#include "pmc_char_array.h"

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Given a sorted vector of floats return the nth different element
 * of that.
 *
 * @param[in] v		vector of float
 * @param[in] ind 	nth different element to identify
 *
 * @return 		value of the element found
 */
extern float find_n_el(float *v, size_t ind);

/**
 * Replace a char in a string.
 *
 * @param[in,out] str	string to manipulate
 * @param[in] origc	character to replace
 * @param[in] newc	new character
 */
extern void char_replace(char *str, char origc, char newc);
/**
 * Compare two floating point numbers.
 *
 * @param[in] a 	first arg to compare
 * @param[in] b 	second arg to compare
 *
 * @return 		1 if a>0 else 0
 */
extern int32_t cmp_float(const void *a, const void * b);

/**
 * Compare two signed int32_t.
 *
 * @param[in] a 	first arg to compare
 * @param[in] b 	second arg to compare
 *
 * @return 		return a - b
 */
extern int32_t cmp_int32(const void *a, const void * b);

/**
 * Find a size_t inside the corresponding vector retunring the index.
 * Return -1 if the element is not present.
 *
 * @param[in] v 	vector in witch to perform the search
 * @param[in] el 	element to find
 * @param[in] maxlen 	stop searching in v after maxlen elements
 *
 * @return		return the index position of the found element
 *			or -1 if the element is not found
 */
extern size_t find_ind_int64(size_t *v, size_t el, size_t maxlen);

/**
 * Find a float inside the corresponding vector returning the
 * index.
 * Return -1 if the element is not present.
 *
 * @param[in] v		vector in witch to perform the search
 * @param[in] el	element to find
 * @param[in] maxlen	stop searching in v after maxlen elements
 *
 * @return		return the index position of the found element
 *			or -1 if the element is not found
 */
extern size_t find_ind_float(float *v, float el, size_t maxlen);

/**
 * Find inside a string a character and return the following position.
 * This func in commonly used to parse a file using the separator char.
 *
 *
 * @param[in] filebuf	string in witch to perform the search
 * @param[in] sep	character to find
 *
 * @return		return the pointer to the following character in the
 * 			buffer
 */
extern char *find_new_el(char *filebuf, char sep);

/**
 * Return a buffer with the rule converted to string
 *
 * @param[in] ed	the input rule
 * @param[in] comb	vector with the variable indexes used by the
 *			edge
 * @param[in] names	pointer to char_array_t containg variables names
 * @param[in] types	array containg variables types
 *
 * @return		allocated buffer with the converted rule
 */
extern char *print_rules(struct edges_t *ed, size_t *comb, char_array_t *names,
			char *types);

/**
 * Given a probability and a covering return the number of seeds needed.
 *
 * @param[in] cov		min covering wanted
 * @param[in] probthresh	detection probability for that covering
 *
 * @return			number of seeds needed
 */
extern size_t estimate_sample(double cov, double probthresh);

/**
 * Read a line from stdin.
 * C makes things simple. :/
 * From http://stackoverflow.com/a/314422/1148634
 *
 * @return 	the buffer containing the readed line
 */
extern char *read_line();

/**
 * Load a text file in to a memory buffer.
 *
 * @param[in] filename		if equal to "-" will load from stdin unless
 *				from the specified filename
 * @param[out] buf_ref		buffer by reference to fill
 * @param[out] size		size of the buffer filled
 *
 * @return			0 on success 1 on error
 */
extern char file_load(char *filename, char **buf_ref, size_t *size);

/**
 * Shift a pointer to the next space or EOF.
 *
 * @param charp_ref	Pointer to shift by reference.
 */
extern void shift_char_p(char **charp_ref);

/**
 * Basic error handler function.
 *
 */
extern void error();

#endif
