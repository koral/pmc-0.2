/**
 * @file		pmc_edges.h
 * @date		Sept 26 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for edges_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_edges_h_included_
#define _pmc_edges_h_included_

#include "pmc_base.h"
#include "pmc_datatype.h"

/**************************************************************************

	Public structure declarations.

**************************************************************************/

struct edges_t {
	size_t ndim;
	double cov;
	double err;
	/* enum data_type_t *type; */
	size_t *comb;
	union elem_32_t *max;
	union elem_32_t *min;
	int32_t nclass;
};

struct edges_collector_t {
	size_t len;
	size_t maxlen;
	struct edges_t *ed[1];
};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Append edges_t to the struct edges_collector_t.
 *
 * @param[in] ed 		pointer to the struct edges_t to add
 * @param[in] nclass		number of the output class
 * @param[in] len_comb		number of variables used by the edge
 * @param[in] cov		covering
 * @param[in] err		error
 * @param[in] comb		vector with the variable indexes used by the
 *				edge
 * @param[in] ncol		same as len_comb!?
 * @param[in] edcoll		pointer to the original struct edges_collector_t
 *
 * @return			struct edges_collector_t returned
 */
extern struct edges_collector_t *append_ed(struct edges_t *ed, int32_t nclass,
					size_t len_comb, double cov,
					double err, size_t *comb,
					size_t ncol,
					struct edges_collector_t
					*edcoll);
/**
 * Resize an struct edges_collector_t with a new size of size elements.
 *
 * @param[in] p 		pointer to the struct edges_collector_t to
 * 				resize
 * @param[in] size 		new size in elements
 *
 * @return 			pointer to the struct edges_collector_t returned
 */
extern
struct edges_collector_t *resize_edges_collector(struct edges_collector_t *p,
						size_t size);
/**
 * Free the struct edges_collector_t.
 *
 * @param[in,out] p 		pointer to the struct edges_collector_t to free
 */
extern void free_edges_collector(struct edges_collector_t *p);

/**
 * Init an struct edges_t.
 *
 * @param[in] ncol	input variables numbers
 *
 * @return		pointer to the struct edges_t allocated
 */
extern struct edges_t *init_edges(size_t ncol);

/**
 * Free and edges_t structure
 *
 * @param[in] ed	pointer to the struct edges_t to free
 */
extern void free_edges(struct edges_t *ed);

#endif
