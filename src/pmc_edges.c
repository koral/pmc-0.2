/**
 * @file		pmc_edges.h
 * @date		Sept 26 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for edges_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include "pmc_edges.h"
#include "pmc_utils.h"

int posix_memalign(void **memptr, size_t alignment, size_t size);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern struct edges_collector_t *append_ed(struct edges_t *ed, int32_t nclass,
					size_t len_comb, double cov, double err,
					size_t *comb, size_t ncol,
					struct edges_collector_t *edcoll)
{
	struct edges_t *edtmp;

	if (edcoll->len == edcoll->maxlen)
		edcoll =
			resize_edges_collector(edcoll, edcoll->maxlen + EDSTEP);
	edtmp = init_edges(ncol);
	edtmp->ndim = len_comb;
	edtmp->nclass = nclass;
	edtmp->cov = cov;
	edtmp->err = err;
	memcpy(edtmp->comb, comb, len_comb * sizeof(*comb));
	memcpy(edtmp->max, ed->max, ncol * sizeof(*ed->max));
	memcpy(edtmp->min, ed->min, ncol * sizeof(*ed->min));
	edcoll->ed[edcoll->len++] = edtmp;

	return edcoll;
}

extern
struct edges_collector_t *resize_edges_collector(struct edges_collector_t *p,
						size_t size)
{
	char tmp = 0;

	if (!p)
		tmp = 1;
	p = realloc(p, size * sizeof(*p->ed) + (sizeof(*p) - sizeof(*p->ed)));
	p->maxlen = size;
	if (tmp)
		p->len = 0;

	return p;
}

extern void free_edges_collector(struct edges_collector_t *p)
{
	size_t i;

	for (i = 0; i < p->len; i++)
		free_edges(p->ed[i]);
	free(p);
}

extern struct edges_t *init_edges(size_t ncol)
{
	struct edges_t *ed;

	ed = malloc(sizeof(*ed));
	if (!ed)
		error();
	ed->ndim = ncol;

	if (posix_memalign((void **)&ed->comb, ALIGNSIZE,
				ncol * sizeof(*ed->comb)))
		error();
	if (posix_memalign((void **)&ed->max, ALIGNSIZE,
				ncol * sizeof(*ed->max)))
		error();
	if (posix_memalign((void **)&ed->min, ALIGNSIZE,
				ncol * sizeof(*ed->min)))
		error();

	return ed;
}

extern void free_edges(struct edges_t *ed)
{
	free(ed->max);
	free(ed->min);
	free(ed->comb);
	free(ed);

	return;
}
