/**
 * @file		pmc_rules.h
 * @date		Sept 26 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for rules_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_rules_h_included_
#define _pmc_rules_h_included_

#include "pmc_base.h"

/**************************************************************************

	Public structure declarations.

**************************************************************************/

struct rules_t {
	size_t len;
	size_t maxlen;
	int32_t *nclass;
	size_t *ncond;
	double *cov;
	double *err;
	char **v;
};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Init the a rules collector.
 *
 * @param[in] nout	number of output classes
 *
 * @return		rules struct initialized
 */
extern struct rules_t *init_rules(int32_t nout);

/**
 * Free a rule collector
 *
 * @param[in,out] rules		struct rules_t to free
 * @param[in] nout		number of input variables
 */
extern void free_rules(struct rules_t *rules, int32_t nout);

/**
 * Change the rules collector size of size elements setting the memory to
 * zero.
 *
 * @param[in,out] rules		struct rules_t to resize
 * @param[in] size		new size
 */
extern void resize_rules(struct rules_t *rules, size_t size);

/**
 * Append a rule in string format to the rules structure.
 * nclass and ncond!?
 *
 * @param[in] str	string containing the rule
 * @param[out] rules	struct rules_t to fill
 */
extern void append_rule(char* str, struct rules_t *rules);

#endif
