/**
 * @file		pmc.c
 * @date		Sept 25 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *			David Vesely <veselda7@gmail.com>
 *
 * @brief		Main file for PMC algorithm
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <argp.h>
#include <time.h>
#include "pmc_learn.h"
#include "pmc_forecast.h"
#include "pmc_api.h"

#ifdef OPENCL
cl_context ctx;
cl_command_queue queue;
cl_device_id device;
long long devtype;
cl_uint computeunits;
cl_mem cled, cllen, clvres, clvsum, clvind;
cl_kernel knl_ReduceTwoStep;
size_t ldim[1];
size_t gdim[1];
cl_kernel *knl_VChunk, *knl_VChunkAdd;
char *knl_text;
cl_mem **clchunks;
size_t oldlen = 0;
char growing = 0;
char fastcl = 0;
#endif

FILE *outfile;
const char *argp_program_version =
	"pmc 0.1";
const char *argp_program_bug_address =
	"<andrea_corallo@yahoo.it>";

/* Program documentation. */
static char doc[] =
	"pmc take the dataset discretized form standard input or file and"
	" extract rules";

/* Description of the arguments we accept. */
static char args_doc[] = "[FILE]";

/* Options we understand. */
static struct argp_option options[] = {
	{"probability", 'p', "NUM", 0, "The rule detection probability."
	 " Default value: "
	 STR(RULEPROBDECT), 0},
	{"verbose", 'v', 0, 0, "Produce verbose output", 0},
	{"interactve", 'i', 0, 0, "Interactive prompt", 0},
	{"autodim", 'a', 0, 0, "Find dinamicaly the max number of "
	 "conditions for the rules.", 0},
	{"quiet", 'q', 0, 0, "Don't produce any progress output", 0},
	{"longnames", 'l', 0, 0, "Produce rules extended names"
	 " adding input info", 0},
	{"mincond", 'm', "NUM", 0, "Minumum number of condition for "
	 "rules. Default value: "
	 STR(MINCONDDEFAULT), 0},
	{"maxcond", 'd', "NUM", 0, "Maximum number of condition for "
	 "rules generated. Default value is the number of the input "
	 "attributes.", 0},
	{"errormax", 'e', "PERC", 0, "Maximum error accepted for "
	 "the rules generated expressend in percentage. Default value: "
	 STR(MAXERRDEFAULT), 0},
	{"mincovering", 'c', "PERC", 0, "Minimum covering accepted "
	 "for rules expressend in percentage. Default value: "
	 STR(MINCOVDEFAULT), 0},
	{"startcovering", 'C', "PERC", 0, "Starting covering threshold"
	 " for rules expressend in percentage. Default value: "
	 STR(STARTCOVDEFAULT), 0},
	{"testpercentage", 't', "PERC", 0, "Percentage of the "
	 "dataset to use for validation prupose. Default value: "
	 STR(VALIDPERCDEFAULT), 0},
	{"output", 'o', "FILE", 0,
	 "Output to FILE instead of standard output", 0},
	{ 0}
};

/**
 * Parse a single option.
 * Get the input argument from argp_parse, which we
 * know is a pointer to our arguments structure.
 *
 * @param[in] key
 * @param[in] arg
 * @param[out] state
 *
 * @return
 */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments_t *arguments = state->input;

	switch (key) {
	case 'p':
		arguments->probability = 1 - atof(arg);
		break;
	case 'm':
		arguments->ncondmin = atoi(arg);
		break;
	case 'd':
		arguments->ncondmax = atoi(arg);
		break;
	case 'e':
		arguments->maxerrperc = atof(arg);
		break;
	case 't':
		arguments->validperc = atof(arg);
		break;
	case 'c':
		arguments->mincov = atof(arg);
		break;
	case 'C':
		arguments->startcovering = atof(arg);
		break;
	case 'i':
		arguments->interactive = 1;
		break;
	case 'l':
		arguments->longname = 1;
		break;
	case 'a':
		arguments->autodim = 1;
		break;
	case 'v':
		arguments->verbose = 1;
		break;
	case 'q':
		arguments->silent = 1;
		break;
	case 'o':
		arguments->outfile = arg;
		break;

	case ARGP_KEY_ARG:
		if (state->arg_num >= 2)
			/* Too many arguments. */
			argp_usage(state);

		arguments->args[state->arg_num] = arg;
		break;

	case ARGP_KEY_END:
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

/* Our argp parser. */
static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

/**
 * Function used to set parameters in interactive mode.
 *
 * @param[out] nextractmin
 * @param[out] ncondmax
 * @param[in]  ncol
 * @param[out] autodim
 * @param[out] mincov
 * @param[out] maxerrperc
 * @param[out] validperc
 */
void interactive_opt(size_t *nextractmin, size_t *ncondmax, size_t ncol,
		char *autodim, double *mincov, double *maxerrperc,
		double *validperc)
{
	char *strtmp;

	printf("Insert the min number of condition ["
		STR(MINCONDDEFAULT)"]: ");
	strtmp = read_line();
	if (strcmp(strtmp, "\n"))
		*nextractmin = atol(strtmp);
	else
		*nextractmin = MINCONDDEFAULT;
	free(strtmp);

	printf("Insert the max number of condition [auto]: ");
	strtmp = read_line();
	if (strcmp(strtmp, "\n")) {
		*ncondmax = atol(strtmp);
	}
	else {
		*ncondmax = ncol;
		*autodim = 1;
	}
	free(strtmp);

	printf("Insert the min covering in percentage ["
		STR(MINCOVDEFAULT)"]: ");
	strtmp = read_line();
	if (strcmp(strtmp, "\n"))
		*mincov = atof(strtmp);
	else
		*mincov = MINCOVDEFAULT;
	free(strtmp);

	printf("Insert the max error in percentage: ["
		STR(MAXERRDEFAULT)"]: ");
	strtmp = read_line();
	if (strcmp(strtmp, "\n"))
		*maxerrperc = atof(strtmp);
	else
		maxerrperc = MAXERRDEFAULT;
	free(strtmp);

	printf("Insert the percentage to use for validation: ["
		STR(VALIDPERCDEFAULT)"]: ");
	strtmp = read_line();
	if (strcmp(strtmp, "\n"))
		*validperc = atof(strtmp);
	else
		*validperc = VALIDPERCDEFAULT;
	free(strtmp);

	return;
}

struct arguments_t init_arguments()
{
	struct arguments_t arguments;

	arguments.probability = 1 - RULEPROBDECT;
	arguments.interactive = 0;
	arguments.silent = 0;
	arguments.verbose = 0;
	arguments.outfile = "-";

	arguments.mincov = MINCOVDEFAULT;
	arguments.startcovering = STARTCOVDEFAULT;
	arguments.maxerrperc = MAXERRDEFAULT;
	arguments.validperc = VALIDPERCDEFAULT;
	arguments.ncondmin = MINCONDDEFAULT;
	arguments.ncondmax = INT32_MAX;
	arguments.autodim = 0;
	arguments.longname = 0;

	return arguments;
}

static void fprintf_time(int silent, clock_t begin, clock_t end)
{
	if (!silent)
		printf("Total time to build rules %f\n",
			(double) (end - begin) / CLOCKS_PER_SEC);
	return;
}


/**
 * Main function.
 *
 * @param[in] 		argc
 * @param[in] 		argv
 *
 * @return		error code
 */
int main(int argc, char **argv)
{
	double cov, mincov, maxerrperc, validperc;
	size_t ncondmax, ncondmin;
	clock_t begin, end;
	struct arguments_t arguments;
	struct data_set_t data;
	size_t *errmaxv;
	FILE *outfile;
	char autodim = 0;
	size_t errtr = 0;
	size_t errval = 0;
	struct edges_collector_t *edcoll = NULL;

	arguments = init_arguments();
	outfile = stdout;

	/* Parse our arguments; every option seen by parse_opt will
	   be reflected in arguments. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	mincov = arguments.mincov;
	cov = arguments.startcovering;
	maxerrperc = arguments.maxerrperc;
	validperc = arguments.validperc;
	ncondmax = arguments.ncondmax;
	ncondmin = arguments.ncondmin;
	autodim = arguments.autodim;

	if (strcmp(arguments.outfile, "-"))
		outfile = fopen(arguments.outfile, "rb");
	if (!outfile)
		error();

#ifdef OPENCL
	init_cl();
#endif
	if (argc == 1)
		arguments.args[0] = "-";
	if (load_data_from_file(&data, arguments.args[0], !arguments.longname,
					arguments.verbose))
		exit(1);

	if (arguments.interactive)
		interactive_opt(&ncondmin, &ncondmax, data.ncol, &autodim,
				&mincov, &maxerrperc, &validperc);
	if (ncondmax > data.ncol)
		ncondmax = data.ncol;

	set_validation_perc(&data, validperc / 100);
	/* find_limits(data.c, data.limits, data.ncol, data.nout); */

	errmaxv = init_err_max(data.nout, data.nout_class, maxerrperc);

#ifdef OPENCL
	load_chunks_to_device(data.c, data.ncol, data.nout);
#endif

	/*---------------------------------------------------------------------
	 *-- LEARNING MODEL ---------------------------------------------------
	 *---------------------------------------------------------------------
	 */
	begin = clock();
	edcoll = learn_pmc_model(&data, cov, mincov, arguments, errmaxv,
				ncondmax, ncondmin, autodim);
	end = clock();
	/*---------------------------------------------------------------------
	 *-- LEARNING MODEL END -----------------------------------------------
	 *---------------------------------------------------------------------
	 */

	fprintf_time(arguments.silent, begin, end);
	fprint_rules(outfile, arguments.silent, data.nout, edcoll, *data.names,
		data.types);

	apply_rules(&errtr, &errval, data.nout, edcoll, data.ncol, data.c);
	fprintf_results(outfile, arguments.silent, validperc, data.nelem, errtr,
			errval);

#ifdef OPENCL
	free_chunks_on_device(data.ncol, data.nout);
#endif
	free_data_set(&data);
	free_edges_collector(edcoll);
	free(errmaxv);

	return 0;
}
