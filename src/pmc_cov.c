/**
 * @file		pmc_cov.c
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for external covering functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include "pmc_cov.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern char covered_fast(register int32_t nout, register struct edges_t *edges,
			register size_t *inds, register size_t ninds,
			register size_t errmax, register struct data_chunk_t ***chunks,
			size_t *count_ref)
{
	register size_t i, k, res;
	register int32_t *pend;
	register int32_t **p;
	register size_t count = 0;

	p = alloca(ninds * sizeof(*p) + 63);
	p = (int32_t **)(((uintptr_t)p + 63)&~(uintptr_t)0x3F);

	for (i = 0; i < ninds; i++)
		p[i] = (int32_t *)chunks[nout][inds[i]]->v;
	pend = (int32_t *)chunks[nout][inds[0]]->v +
		chunks[nout][inds[0]]->trainlen;

	while (*p < pend) {
		res = 0;
		for (i = 0; i < ninds; i++) {
			k = inds[i];
			if ((edges->min[k].i <= *p[i] &&
					edges->max[k].i >= *p[i]) ||
				*p[i] == MISSVAL)
				res++;
			p[i]++;
		}
		if (res == ninds && count++ == errmax)
			return 1;
	}
	*count_ref = count;

	return 0;
}

extern size_t covered(register int32_t nout, register struct edges_t *edges,
		register size_t *inds, register size_t ninds,
		register struct data_chunk_t ***chunks)
{
	register size_t i, k, res;
	register int32_t *pend;
	register int32_t **p;
	register size_t count = 0;

	p = alloca(ninds * sizeof(*p) + 63);
	p = (int32_t **)(((uintptr_t)p + 63)&~(uintptr_t)0x3F);

	for (i = 0; i < ninds; i++)
		p[i] = (int32_t *)chunks[nout][inds[i]]->v;
	pend = (int32_t *)chunks[nout][inds[0]]->v +
		chunks[nout][inds[0]]->trainlen;

	while (*p < pend) {
		res = 0;
		for (i = 0; i < ninds; i++) {
			k = inds[i];
			if (*p[i] == MISSVAL ||
				(edges->min[k].i <= *p[i] &&
					edges->max[k].i >= *p[i]))
				res++;
			p[i]++;
		}
		if (res == ninds)
			count++;
	}

	return count;
}
