/**
 * @file		pmc_ind.h
 * @date		Aug 29 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file  for functions handling indexes.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_ind_h_included_
#define _pmc_ind_h_included_

#include "pmc_dataset.h"

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Init a char matrix (index data matrix).
 *
 * @param[in] data	Struct data_set_t containing the dataset.
 * @param[in] nsample	Number of seeds.
 *
 * @return		Index matrix returned.
 */
extern char **init_chunks_ind(const struct data_set_t *data,
			size_t nsample);

/**
 * Set the memory of the index data matrix.
 * If nsample is -1 all the values are setted to 1
 * else are setted to 1 only nsample values.
 *
 * @param[in,out] ind 	Char index data matrix.
 * @param[in] data	Struct data_set_t containing the dataset.
 * @param[in] nsample	Number of seed.
 *			If is -1 all the values are setted to 1
 */
extern void set_chunks_ind(char **ind, const struct data_set_t *data,
			size_t nsample);

/**
 * Given two index data matrices perform the logical operation
 * ind1 = ind1 AND ind2
 * in every element.
 *
 * @param[in,out] ind1		First index data matrix.
 * @param[in] ind2		Second index data matrix.
 * @param[in] data		Struct data_set_t containing the dataset.
 */
extern void ind1_eq_ind2_and_ind2(char** ind1, char** ind2,
				const struct data_set_t *data);

/**
 * Given an index data matrix count the number of pattern present in it.
 *
 * @param[in] ind		Index data matrix.
 * @param[in] data		Struct data_set_t containing the dataset.
 *
 * @return
 */
extern size_t count_ind(char** ind, const struct data_set_t *data);

/**
 * Free a char matrix (index data matrix).
 *
 * @param[in,out] cind		Char index data matrix.
 * @param[in] data		Struct data_set_t containing the dataset.
 */
extern void free_chunks_ind(char **cind, const struct data_set_t *data);

/**
 * Given a set of rules and an index data matrix drops the points in it.
 *
 * @param[in,out] cind		Char index data matrix.
 * @param[in] edcoll		Struct edges_collector_t storing the set of
 *				rules.
 * @param[in] data		Struct data_set_t containing the dataset.
 */
extern void drop_points_all_rules(char **cind,
				struct edges_collector_t *edcoll,
				const struct data_set_t *data);

/**
 * Given two index data matrices return the first seed to use
 *
 * @param[in] cind1		First char index data matrix.
 * @param[in] cind2		Second char index data matrix.
 * @param[in] curr_out		Output classes number.
 * @param[in] comb		Vector with the variable indexes used.
 * @param[in] len_comb		Number of variable in the combo.
 * @param[in] data		Struct data_set_t containing the dataset.
 *
 * @return			The index number of the seed or
 *				-1 if none is found.
 */
extern size_t find_first_seed(char **cind1, char **cind2, int32_t curr_out, size_t *comb,
			size_t len_comb,  const struct data_set_t *data);

/**
 * Given a rule and an index data matrix drops the points in it.
 *
 * @param[in,out] cind		Index data matrix in which dropping points.
 * @param[in] curr_out		Output classes number.
 * @param[in] edges		Struct edges_t carrying the rule.
 * @param[in] inds		Vector with the variable indexes used by the
 *				edge.
 * @param[in] ninds		Number of variable in the combo inds.
 * @param[in] data		Struct data_set_t containing the dataset.
 */
extern void drop_points(char **cind, int32_t curr_out, struct edges_t *edges, size_t *inds,
			size_t ninds,  const struct data_set_t *data);

/**
 * Load and return the index data matrix from hadoop cache file.
 * Used in the map reduce cycle.
 *
 * @param[in] data		Struct data_set_t containing the dataset.
 *
 * @return 			Index data matrix.
 */
extern char **load_ind_from_cache(const struct data_set_t *data);

#endif
