/**
 * @file		pmc_core.c
 * @date		Sept 25 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *			David Vesely <veselda7@gmail.com>
 *
 * @brief		Implementation file for PMC algorithm core functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_core.h"

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 * Given a seed find a single rule ad append to the edge collector.
 *
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] ed		Struct edges_t to use for the rule.
 * @param[in] curr_out		Current index of the output class.
 * @param[in] len_comb		Number of variable in the combo.
 * @param[in] comb		Vector with the variable indexes used by the
 *				edge.
 * @param[in,out] cind		Char index data matrix.
 * @param[in] cindext		External char index data matrix.
 * @param[in] pattind		Index of the seed to use.
 * @param[in] mincov		Minimum covering for accepting a rule.
 * @param[in,out] edcoll_ref	Struct edges_collector_t by reference in which
 *				storing rules.
 * @param[in] errmaxv		Maximum error to accept a rule.
 *
 * @return			0 or 1 if the overdim check is used and reached.
 */
static char find_rule(const struct data_set_t *data, struct edges_t *ed,
		int32_t curr_out, size_t len_comb, size_t *comb, char **cind,
		char **cindext, size_t pattind, double mincov,
		struct edges_collector_t **edcoll_ref, size_t *errmaxv);
/**
 * Core function of pmalgo.
 * Given a seed and a data set find a rule if acceptable.
 *
 * @param[in,out] edges		Struct edges_t to store the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output classes number.
 * @param[in] comb		Vector with the variable indexes used by the
 *				rule.
 * @param[in] len_comb		Number of variable indexes in comb.
 * @param[in] patt_ind		Index of the current seed.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				machables.
 *
 * @return			0 if the rule is accettable else 1.
 */
static char find_edges(struct edges_t *edges, const struct data_set_t *data,
		int32_t curr_out, size_t *comb, size_t len_comb,
		size_t patt_ind, size_t *errmaxv);

/**
 * Core function of pmalgo.
 * Finds the top of cube edges explaining the borders of a rule.
 *
 * @param[in] patt_ind		Index of the current seed.
 * @param[in,out] edges		Struct edges_t to store the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output classes number.
 * @param[in] comb		Vector with the variable indexes used by the
 *				rule.
 * @param[in] len_comb		Number of variable indexes in comb.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				machables.
 * @param[in,out] steps		Struct edges_t to store the steps of finding
 *				edges.
 * @param[out] dimtouched	The touched dimension of the rule (edges).
 * @param[out] min_max
 */
static void grow_inc_edges(size_t patt_ind, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps, size_t * dimtouched,
			enum min_max_t *min_max);

/**
 * Core function of pmalgo.
 * Finds a the most possible bottom cube edges explaining the borders of a rule.
 *
 * @param[in] patt_ind		Index of the current seed.
 * @param[in,out] edges		Struct edges_t to store the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output classes number.
 * @param[in] comb		Vector with the variable indexes used by the
 *				rule.
 * @param[in] len_comb		Number of variable indexes in comb.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				machables.
 * @param[in,out] steps		Struct edges_t to store the steps of finding
 *				edges.
 */
static void grow_dec_edges(size_t patt_ind, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps);

/**
 * Verify if a rule reach the limit of the data space.
 *
 * @param[in] inds	Vector with the variable indexes used by the
 *			edge.
 * @param[in] nind	Number of variable indexes in inds.
 * @param[in] edges	Struct edges_t with the edges of the rule.
 * @param[in] limits	Limits of the current data matrix.
 *
 * @return		1 when the rule is accepted unless 0.
 */
static char verify_over_dim(size_t *inds, size_t nind, struct edges_t *edges,
			struct edges_t *limits);

/**
 * Decide if we can accept a rule.
 * Return 1 when the rule is accepted unless 0.
 *
 * @param[in] edges		Struct edges_t carrying the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output class number of the rule.
 * @param[in] comb		Vector with the variable indexes used by the
 *				edge.
 * @param[in] len_comb		Number of variable in the combo inds.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				mached by the rule.
 *
 * @return			1 when the rule is accepted unless 0.
 */
static char verify_ed(struct edges_t *edges, const struct data_set_t *data,
		int32_t curr_out, size_t *comb, size_t len_comb,
		size_t *errmaxv);

/**
 * Increase the most possible bottom cube edge explaining the border.
 * If possible increase the step size.
 *
 * @param[in] patt_ind		Index of the current seed.
 * @param[in] i			Index number of the current column.
 * @param[in,out] edges		Struct edges_t to store the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output classes number.
 * @param[in] comb		Vector with the variable indexes used by the
 *				rule.
 * @param[in] len_comb		Number of variable indexes in comb.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				machables.
 * @param[in,out] steps		Struct edges_t to store the steps of finding
 *				edges.
 * @param [out] res_ref		If the edge has touched is setted to 0.
 * @param [out] min_max		Set the min_max_t depending where the edge has
 *				touched.
 *
 * @return			-1 if the edge excede the limits 0 otherwise.
 */
static int grow_inc_edge(size_t patt_ind, size_t i, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps, size_t *res_ref,
			enum min_max_t *min_max);
/**
 * Increase the most possible bottom cube edge explaining the border.
 * If not possible reduce the step size.
 *
 * @param[in] patt_ind		Index of the current seed.
 * @param[in] i			Index number of the current column.
 * @param[in,out] edges		Struct edges_t to store the rule.
 * @param[in] data		Struct data_set_t to use as dataset.
 * @param[in] curr_out		Current output classes number.
 * @param[in] comb		Vector with the variable indexes used by the
 *				rule.
 * @param[in] len_comb		Number of variable indexes in comb.
 * @param[in] errmaxv		Max number of pattern of the other classes
 *				machables.
 * @param[in,out] steps		Struct edges_t to store the steps of finding
 *				edges.
 */
static void grow_dec_edge(size_t patt_ind, size_t i, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern size_t find_rules(const struct data_set_t *data,
			size_t *comb,
			double mincov,
			struct edges_collector_t **edcoll_ref,
			size_t len_comb,
			size_t *errmaxv,
			char **cindext)
{
	int32_t i;
	struct edges_t *ed;
	char **cind;
	size_t pattind;
	size_t stepcount = 0;

	for (i = 0; i < data->nout; i++) {
		ed = init_edges(data->ncol);
		cind = init_chunks_ind(data, -1);
		pattind =
			find_first_seed(cind, cindext, i, comb, len_comb, data);
		while (pattind != -1) {
#ifdef HADOOP
			hadoop_increment_rule_count();
#endif
			stepcount++;
			if (find_rule(data, ed, i, len_comb, comb, cind,
					cindext, pattind, mincov, edcoll_ref,
					errmaxv))
				break;
			pattind = find_first_seed(cind, cindext, i, comb,
						len_comb, data);
		}
		/* reduce external seeds */
		/* ind1_eq_ind2_and_ind2(cindext, cind, data->nout, data->c); */

		free_chunks_ind(cind, data);
		free_edges(ed);
	}
	return stepcount;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static char find_rule(const struct data_set_t *data, struct edges_t *ed,
		int32_t curr_out, size_t len_comb, size_t *comb, char **cind,
		char **cindext, size_t pattind, double mincov,
		struct edges_collector_t **edcoll_ref, size_t *errmaxv)
{
	char res;
	size_t i, j;
	double cov;
	size_t errcount = 0, othercount = 0;
	double err = 0;

	res = find_edges(ed, data, curr_out, comb,
			len_comb, pattind, errmaxv);
	DROPPOINTS(cind, curr_out, ed, comb, len_comb, data);
	if (!res) {
		if (!verify_over_dim(comb, len_comb, ed, data->limits)) {
			return 0; /* put 1 to activate overdim check */
		}

		cov = 100 * (double) COVERED(curr_out, data->nout, ed, comb,
					len_comb, data->c) /
			data->c[curr_out][0]->trainlen;
		for (i = 0; i < data->nout; i++)
			if (i != curr_out) {
				errcount += COVERED(i, data->nout, ed, comb,
						len_comb, data->c);
				othercount += data->c[i][0]->trainlen;
			}
		err = 100 * (double) errcount / othercount;
		if (cov >= mincov) {
			for (i = 0; i < len_comb; i++) {
				j = comb[i];
				if (ed->min[j].i == data->limits->min[j].i)
					ed->min[j].i = MINVAL;
				if (ed->max[j].i == data->limits->max[j].i)
					ed->max[j].i = MAXVAL;
				DROPPOINTS(cindext, curr_out, ed, comb,
					len_comb, data);
			}
			*edcoll_ref =
				append_ed(ed, curr_out, len_comb, cov, err,
					comb, data->ncol, *edcoll_ref);
		}

	}
	return 0;
}

static char find_edges(struct edges_t *edges, const struct data_set_t *data,
		int32_t curr_out, size_t *comb, size_t len_comb,
		size_t patt_ind, size_t *errmaxv)
{
	size_t i, j, dimtouched;
	struct edges_t *steps;
	enum min_max_t min_max = UNKNOWN;

	for (i = 0; i < len_comb; i++) {
		j = comb[i];
		edges->min[j].i = edges->max[j].i =
			data->c[curr_out][j]->v[patt_ind].i;
	}
	steps = init_edges(data->ncol);
	for (i = 0; i < len_comb; i++) {
		j = comb[i];
		steps->max[j].i = steps->min[j].i = 1;
	}
#ifdef OPENCL
	growing = 1;
#endif

	grow_inc_edges(patt_ind, edges, data, curr_out, comb, len_comb,
		errmaxv, steps, &dimtouched, &min_max);
#ifdef OPENCL
	growing = 0;
#endif
	for (i = 0; i < len_comb; i++) {
		j = comb[i];
		if (!(j == dimtouched && min_max == MIN))
			steps->min[j].i =
				abs((data->limits->max[j].i -
						data->limits->min[j].i)
					/ FRACTSTEP);
		if (!(j == dimtouched && min_max == MAX))
			steps->max[j].i =
				abs((data->limits->max[j].i -
						data->limits->min[j].i))
				/ FRACTSTEP;
	}
	grow_dec_edges(patt_ind, edges, data, curr_out, comb, len_comb, errmaxv,
		steps);
	free_edges(steps);
	if (verify_ed(edges, data, curr_out, comb, len_comb, errmaxv))
		return 0;
	else
		return 1;
}

static void grow_inc_edges(size_t patt_ind, struct edges_t *edges,
			const struct data_set_t *data,
			int32_t curr_out, size_t *comb, size_t len_comb,
			size_t *errmaxv, struct edges_t *steps,
			size_t *dimtouched, enum min_max_t *min_max)
{
	size_t i, j;
	size_t res = 1;
	*min_max = UNKNOWN;

	while (res) {
		res = 0;
		for (i = 0; i < len_comb; i++) {
			j = comb[i];
			if (grow_inc_edge(patt_ind, j, edges, data, curr_out,
						comb, len_comb, errmaxv, steps,
						&res, min_max))
				break;
		}
	}
	*dimtouched = j;
}

static void grow_dec_edges(size_t patt_ind, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps)
{
	size_t i, j;
	size_t res = 1;

	while (res) {
		for (i = 0; i < len_comb; i++) {
			j = comb[i];
			grow_dec_edge(patt_ind, j, edges, data, curr_out, comb,
				len_comb, errmaxv, steps);
		}
		res = 2 * len_comb;
		for (i = 0; i < len_comb; i++) {
			j = comb[i];
			if (steps->min[j].i == 0)
				res--;
			if (steps->max[j].i == 0)
				res--;
		}
	}
}

static char verify_over_dim(size_t *inds, size_t nind, struct edges_t *edges,
			struct edges_t *limits)
{
	size_t i, j;

	for (i = 0; i < nind; i++) {
		j = inds[i];
		if (edges->min[j].i == limits->min[j].i &&
			edges->max[j].i == limits->max[j].i)
			return 0;
	}

	return 1;
}

static char verify_ed(struct edges_t *edges, const struct data_set_t *data,
		int32_t curr_out, size_t *comb, size_t len_comb,
		size_t *errmaxv)
{
	size_t i, errtmp, count; /* a, b; */

	if (errmaxv[curr_out])
		errtmp = errmaxv[curr_out] *
			COVERED(curr_out, data->nout, edges, comb, len_comb,
				data->c)
			/ data->c[curr_out][0]->trainlen;
	else
		errtmp = 0;

	for (i = 0; i < data->nout; i++)
		if (i != curr_out) {
			/* a = covered_cl(i, data->nout, edges, comb, len_comb,
			   errmax, data->c); */
			/* b = covered_fast(i, edges, comb, len_comb, errmax,
			   data->c); */
			/* if (a) */
			/*   a = 1 ; */
			/* if (a != b) */
			/*   printf("mumble..."); */
			if (COVEREDFAST(i, data->nout, edges, comb, len_comb,
						errtmp, data->c, &count)
				!= 0)
				return 0;
			errtmp -= count;
		}

	return 1;
}

static int grow_inc_edge(size_t patt_ind, size_t i, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps, size_t *res_ref,
			enum min_max_t *min_max)
{
	union elem_32_t ed_min, ed_max, seed;

	seed.i = data->c[curr_out][i]->v[patt_ind].i;

	ed_min.i = edges->min[i].i - steps->min[i].i;

	if (data->limits->min[i].i <= ed_min.i) {
		*res_ref = 1;
		edges->min[i].i = ed_min.i;
		if (ed_min.i > seed.i ||
			!verify_ed(edges, data, curr_out, comb, len_comb,
				errmaxv)) {
			edges->min[i].i += steps->min[i].i;
			*min_max = MIN;
			*res_ref = 0;
		} else {
			steps->min[i].i *= 2;
		}
	}
	if (edges->min[i].i <= data->limits->min[i].i) {
		*min_max = MIN;
		*res_ref = 0;
		return -1;
	}

	ed_max.i = edges->max[i].i + steps->max[i].i;

	if (ed_max.i <= data->limits->max[i].i) {
		*res_ref = 1;
		edges->max[i].i = ed_max.i;
		if (ed_max.i < seed.i ||
			!verify_ed(edges, data, curr_out, comb, len_comb,
				errmaxv))
		{
			edges->max[i].i -= steps->max[i].i;
			*min_max = MAX;
			*res_ref = 0;
		} else {
			steps->max[i].i *= 2;
		}
		if (edges->max[i].i >= data->limits->max[i].i) {
			*min_max = MAX;
			*res_ref = 0;
		        return -1;
		}
	}

	return 0;
}


static void grow_dec_edge(size_t patt_ind, size_t i, struct edges_t *edges,
			const struct data_set_t *data, int32_t curr_out,
			size_t *comb, size_t len_comb, size_t *errmaxv,
			struct edges_t *steps)
{
	union elem_32_t ed_min, ed_max, seed;

	seed.i = data->c[curr_out][i]->v[patt_ind].i;

	ed_min.i = edges->min[i].i - steps->min[i].i;

	if (steps->min[i].i != 0 &&
		data->limits->min[i].i <= ed_min.i) {
		edges->min[i].i = ed_min.i;
		if (ed_min.i > seed.i ||
			!verify_ed(edges, data, curr_out, comb, len_comb,
				errmaxv))
		{
			edges->min[i].i += steps->min[i].i;
			steps->min[i].i /= 2;
		}
	} else {
		steps->min[i].i /= 2;
	}

	ed_max.i = edges->max[i].i + steps->max[i].i;

	if (steps->max[i].i != 0 &&
		ed_max.i <= data->limits->max[i].i) {
		edges->max[i].i = ed_max.i;
		if (ed_max.i < seed.i ||
			!verify_ed(edges, data, curr_out, comb, len_comb,
				errmaxv))
		{
			edges->max[i].i -= steps->max[i].i;
			steps->max[i].i /= 2;
		}
	} else {
		steps->max[i].i /= 2;
	}

	return;
}
