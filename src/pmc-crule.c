/**
 * @file		pmc-crule.c
 * @date		Dec 5 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Convert Rules in non discretized format
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <argp.h>
#include <time.h>
#include "pmc_base.h"
#include "pmc_dataset.h"
#include "pmc_edges.h"

/* Declaration of the parse function inplemented in pmc_lexer.c */
int parse(char *filename);

struct edges_collector_t *edcoll;
char_array_t names;
size_t *ind;

const char *argp_program_version =
	"pmc-crule 0.1";
const char *argp_program_bug_address =
	"<andrea_corallo@yahoo.it>";
/* Program documentation. */
static char doc[] =
	"pmc-crule convert rules in non discretized format. \n"
	"By default take discretized rules from standard input and write to "
	"standard output";
/* Description of the arguments we accept. */
static char args_doc[] = "FILE";
/* Options we understand. */
static struct argp_option options[] = {
	{"verbose",  'v', 0,      0,  "Produce verbose output" , 0},
	{"names",  'n', 0,      0,  "Takes input names from line 2" , 0},
	{"separator",'s', "'SEPARATOR_CHAR'",      0,
	 "Specify the separator character. Default is comma", 0},
	{"inputrules",   'i', "FILE", 0,
	 "Specify a file to load rules to convert" , 0},
	{"missing",  'm', "'MISSING_STRING'",      0,
	 "Specify the missing string. Default is ?", 0},
	{"output",   'o', "FILE", 0,
	 "Output to FILE instead of standard output" , 0},
	{ 0 }
};
/* To communicate with parse_opt. */
struct arguments_t {
	char *args[1];		/* arg1  */
	int verbose, names;
	char *infile, *outfile, *miss, *sep;
};

/**
 * Parse a single option.
 * Get the input argument from argp_parse, which we
 * know is a pointer to our arguments structure.
 *
 * @param[in] key
 * @param[in] arg
 * @param[out] state
 *
 * @return
 */
static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
	/* Get the input argument from argp_parse, which we
	   know is a pointer to our arguments structure. */
	struct arguments_t *arguments = state->input;

	switch (key)
	{
	case 'v':
		arguments->verbose = 1;
		break;
	case 'o':
		arguments->outfile = arg;
		break;
	case 'n':
		arguments->names = 1;
		break;
	case 's':
		arguments->sep = arg;
		break;
	case 'm':
		arguments->miss = arg;
		break;
	case 'i':
		arguments->infile = arg;
		break;
	case ARGP_KEY_ARG:
		if (state->arg_num >= 1)
			/* Too many arguments. */
			argp_usage (state);
		arguments->args[state->arg_num] = arg;
		break;
	case ARGP_KEY_END:
		if (state->arg_num < 1)
			/* Not enough arguments. */
			argp_usage (state);
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}
/* argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0};

/**
 * Main function for pmc-rule.
 *
 * @param[in] argc
 * @param[in] argv
 *
 * @return
 */
int main(int argc, char **argv)
{
	char sepc;
	struct arguments_t arguments;
	char_array_t carray, longnames;
	size_t i, j, k, ncol, nline;
	char *type, *buf;
	struct edges_t *ed;
	union elem_32_t *elemv;
	union elem_32_t **col;

	arguments.names = 0;
	arguments.verbose = 0;
	arguments.outfile = NULL;
	arguments.infile = NULL;
	arguments.sep = ",";
	arguments.miss = "?";

	/* Parse our arguments; every option seen by parse_opt will
	   be reflected in arguments. */
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	init_char_array(&carray);


	if (!strcmp(arguments.sep, "\\t"))
		arguments.sep = "\t";
	sepc = arguments.sep[0];

	load_heterogeneous_file(arguments.args[0], &ncol, &type, sepc, &ind,
				&nline, &longnames, arguments.names, 1,
				arguments.miss, &col, &carray);

	parse(arguments.infile);

	nline = nline - arguments.names - 1;
	elemv = malloc((nline)*sizeof(*elemv));

	/* convert rules */

	for (j = 0; j < ncol; j++) {
		if (type[j]=='f') {
			memcpy(elemv, col[ind[j]], nline * sizeof(*elemv));
			qsort(elemv, nline, sizeof(*elemv), cmp_float);
			for (i = 0; i < edcoll->len; i++) {
				ed = edcoll->ed[i];
				for (k = 0; k < ed->ndim; k++)
					if (ed->comb[k] == j) {
						if (ed->min[j].i != MINVAL)
							ed->min[j].e = enc_f(
								find_n_el(
								(float *)elemv,
								ed->min[j].i));
						/* elemv[ed->min[j].i]; */
						if (ed->max[j].i != MAXVAL)
							ed->max[j].e = enc_f(
								find_n_el(
								(float *)elemv,
								ed->max[j].i));
						/* ed->max[j] =
						   elemv[ed->max[j].i]; */
					}
			}
		}
	}
	printf("New rules:\n");
	for (i = 0; i < edcoll->len; i++) {
		ed = edcoll->ed[i];
		buf = print_rules(ed, ed->comb, &names, NULL);
		printf("%s", buf);
		free(buf);
	}


	free_edges_collector(edcoll);
	free_char_array(&carray);
	free_char_array(&names);
	free_char_array(&longnames);
	free(type);
	free(ind);


	return 0;
}
