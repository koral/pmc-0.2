/**
 * @file		pmc_char_array.h
 * @date		Sept 01 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Hearder file for function related to struct
 *			char_array_t.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_char_array_h_included_
#define _pmc_char_array_h_included_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRINGSTEP (1 << 10)
#define MAXSTRING (1 << 10)
void error();
/* all previous are to be removed */

/**************************************************************************

	Public structure declarations.

**************************************************************************/

/** Structure for holding array of strings. */
typedef struct {
	/** Actual len of the index array. */
	size_t len;
	/** Max len the index array. */
	size_t maxlen;
	/** Index array of pointer to char. */
	char **c;
} char_array_t;


/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Set the initial values of char_array_t fields
 *
 * @param[in,out] carray	char_array_t to init
 */
extern void init_char_array(char_array_t *carray);

/**
 * Free the char_array_t struct.
 *
 * @param[in,out] carray 	char_array_t to free
 */
extern void free_char_array(char_array_t *carray);

/**
 * Change the char_array_t size of size elements setting the memory to zero.
 *
 * @param[in,out] carray	char_array_t to resize
 * @param[in] size		new size
 */
extern void resize_char_array(char_array_t *carray, size_t size);

/**
 * Add a string to a char_array_t.
 *
 * @param[in] str		string to add
 * @param[in,out] carray	the char_array_t to fill
 * @param[in] checkdup		if not NULL check if the string is already
 * 				present
 *
 * @return			the new length of the char_array_t
 */
extern size_t add_str_to_char_array(char *str, char_array_t *carray,
					    char checkdup);

/**
 * Given a string find if is present in the char_array_t.
 *
 * @param[in] carray	char_array_t in wich perform the search
 * @param[in] str	string to search
 *
 * @return		return the index of the element if found else SIZE_MAX
 */
size_t find_el_char_array(char_array_t *carray, char *str);


#endif
