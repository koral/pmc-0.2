/**
 * @file		pmc_dataset.h
 * @date		Aug 28 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for dataset struct and related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_dataset_h_included_
#define _pmc_dataset_h_included_

#include "pmc_chunks.h"

/**************************************************************************

	Public structure declarations.

**************************************************************************/

/** Structure for holding the entire dataset and some information related. */
struct data_set_t {
	/** Number of input variables in the dataset matrix. */
	size_t ncol;
	/** Number of output classes. */
	uint32_t nout;
	/** Total number of input patterns (lines). */
	size_t nelem;
	/** Array containing the frequency of the different classes */
	size_t *nout_class;
	/** Output classes and theier labes. */
	/** Contains also the number of outputs. */
	char_array_t *out;
	/** Names of the input features. */
	char_array_t *names;
	/** Types of the input features. */
	char *types;
	/** Limits of the dataset space. */
	struct edges_t *limits;
	/** Chunks with data. */
	struct data_chunk_t ***c;

};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Load data from a buffer and fill the data_set_t structure.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to fill.
 * @param[in] data_buf		ASCII data buffer to parse and load.
 * @param[in] buf_size		Size of the buffer. If -1 will be calculated
 *				again in the func.
 * @param[in] shortname		Shortname flag (obsloete?).
 * @param[in] verbose		Verbose flag.
 *
 * @return			Returns 0 on success or -1 on error.
 */
extern int load_data_from_buf(struct data_set_t *data, char *data_buf,
			size_t buf_size, int shortname, int verbose);

/**
 * Load data from file and fill the data_set_t structure.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to fill.
 * @param[in] filename		Filename to load.
 * @param[in] shortname		Shortname flag (obsloete?).
 * @param[in] verbose		Verbose flag.
 *
 * @return			Returns 0 on success or -1 on error.
 */
extern int load_data_from_file(struct data_set_t *data, char *filename,
			int shortname, int verbose);
/**
 * Load a dataset form hadoop cache file.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to fill.
 * @param[in] comb		Variable indexes that we are going to load.
 * @param[in] len_comb		Total number of variables in the current
 *				combination of variable that we are loading.
 * @param[in] nout		Total number of output classes.
 *
 * @return			Returns 0 on success or -1 on error.
 */
extern int load_data_form_cache(struct data_set_t *data, size_t *comb,
				size_t len_comb, uint32_t nout);

/**
 * Function for setting the percentage of the dataset to use as validation set.
 * Updates also the the space limits information in the limits field.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to use.
 * @param[in] validperc		Percentage of data to use for validation purpose
 *				(expressed from 0 to 1).
 */
extern void set_validation_perc(struct data_set_t *data, double validperc);

/**
 * Function for freeing the a data_set_t structure.
 *
 * @param[in,out] data		Pointer to the struct data_set_t to free
 */
extern void free_data_set(struct data_set_t *data);

/**
 * Given a filename load (also non homogeneous data).
 * Used in pmc-rule (OBSOLETE).
 *
 * @param[in] filename		filename to open
 * @param[out] ncol_ref		total number of input varialbes by reference
 * @param[out] type_ref		type array passed by reference by reference
 * @param[in] sepc		separator character
 * @param[out] ind_ref		index vector passed by reference
 * @param[out] nline_ref	line number  passed by reference
 * @param[out] names_ref	variable names in char_array_t struct by
 *				reference
 * @param[in] argnames		if true load names for output classes
 * @param[in] argdiscint	obsolete!?
 * @param[in] argmiss		missing value character (by reference!?)
 * @param[out] col_ref		data matrix of elem_32_t struct passed by
 *				reference
 * @param[out] carray_ref	output classes names char_array_t by reference
 */
extern void load_heterogeneous_file(char *filename, size_t *ncol_ref,
				char **type_ref, char sepc, size_t **ind_ref,
				size_t *nline_ref, char_array_t *names_ref,
				int argnames, int argdiscint, char *argmiss,
				union elem_32_t ***col_ref,
				char_array_t *carray_ref);


#endif
