/**
 * @file		pmc_cl.h
 * @date		Oct 3 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for PMC algorithm ported in OpenCL.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_cl_h_included_
#define _pmc_cl_h_included_

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Initialize the pmc OpenCL system.
 * Choose the device and compile the kernels.
 *
 */
extern void init_cl();

/**
 * Load data to the OpenCl device.
 *
 * @param[in] chunks	chunk data matrix
 * @param[in] ncol	input variables numbers
 * @param[in] nout	output classes number
 *
 * @return		0
 */
extern char load_chunks_to_device(struct data_chunk_t ***chunks, size_t ncol,
			int32_t nout);

/**
 * Free data on the OpenCl device.
 *
 * @param[in] ncol	number of variables in the data matrix
 * @param[in] nout	output classes number
 *
 * @return		0
 */
char free_chunks_on_device(size_t ncol, int32_t nout);

/**
 * Optimized function for covering computation.
 * Return 1 if covering >= errmax or 0.
 *
 * @param[in] nout		current output number
 * @param[in] nouttot		output classes number
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] inds		vector with the variable indexes used by the
 *				edge
 * @param[in] ninds		number of variable in the combo inds
 * @param[in] countmax		max number of pattern covered
 * @param[in] chunks		chunk data matrix
 *
 * @return			0 if the covered pattern are under countmax
 *				else the actual covered pattern
 */
extern size_t covered_cl_fast(int32_t nout, int32_t nouttot,
			struct edges_t *edges, size_t *inds, size_t ninds,
			size_t countmax, struct data_chunk_t ***chunks);

/**
 * Covering computation using OpenCl.
 *
 * @param[in] nout		current output class
 * @param[in] nouttot		total number of output classes
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] inds		vector with the variable indexes used by the
 *				edge
 * @param[in] ninds		number of variable in the combo inds
 * @param[in] chunks		chunk data matrix
 *
 * @return			the number of covered pattern
 */
extern size_t covered_cl(int32_t nout, int32_t nouttot, struct edges_t *edges,
			size_t *inds, size_t ninds,
			struct data_chunk_t ***chunks);

/**
 * Apply a rule usign the OpenCl device.
 *
 * @param[in,out] clv		data index vector
 * @param[in] nout		current output class
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] inds		vector with the variable indexes used by the
 *				edge
 * @param[in] ninds		number of variable in the combo inds
 * @param[in] additive		if true old values in vindex vector are
 *				preserved when true
 */
extern void apply_rule_cl(cl_mem clv, int32_t nout, struct edges_t *edges,
			size_t *inds, size_t ninds, char additive);

/**
 * Given a rule drop points in the data index matrix using OpenCl.
 *
 * @param[in,out] cind		data index vector
 * @param[in] nout		current output class
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] inds		vector with the variable indexes used by the
 *				edge
 * @param[in] ninds		number of variable in the combo inds
 * @param[in] chunks		chunk data matrix
 */
extern void drop_points_cl(char **cind, int32_t nout, struct edges_t *edges,
			size_t *inds, size_t ninds,
			struct data_chunk_t ***chunks)

#endif
