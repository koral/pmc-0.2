/**
 * @file		pmc_base.h
 * @date		Sept 25 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for PMC algorithm with all the basic
 *			definitions
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_base_h_included_
#define _pmc_base_h_included_

#include <limits.h>
#include <sys/types.h>

/**************************************************************************

	PMC definitions

**************************************************************************/

#define DEF2STR(x) #x
#define STR(x) DEF2STR(x)

#if ( __WORDSIZE == 64 )
#define SIZE_T_P %lu
#define CHUNKFILENAME chunk-%u-%lu.bin
#define INPUTDEFAULTNAME Inp%lu
#define RULESFILENAME2 -%lu
#endif
#if ( __WORDSIZE == 32 )
#define SIZE_T_P %u
#define CHUNKFILENAME chunk-%u-%u.bin
#define INPUTDEFAULTNAME Inp%u
#define RULESFILENAME2 -%u
#endif

#ifndef HADOOP
#define TEMPFILEDIR tmp/
#else
#define TEMPFILEDIR ./
#endif

#define CHUNKINDFILENAME chunkind-%u.bin
#define HDINPUTFILENAME input.txt
#define RULESFILENAME1 rules
#define RULESFILENAME3 .log
#define MINCONDDEFAULT 1
#define VALIDPERCDEFAULT 0
#define STARTCOVDEFAULT 10
#define MINCOVDEFAULT 5
#define MAXERRDEFAULT 0
#define STRINGSTEP (1 << 10)
#define EDSTEP (1 << 10)
#define MAXCLASSOUT (1 << 9)
#define ALIGNSIZE (1 << 6)
#define FRACTSTEP 1 /* (1 << 2) */
#define MAXSTRING (1 << 10)
#define THRESHOLD 0.1
#define MAXCONDCL 8
#define MAXCONDTEST 64
#define WAVEFRONT 64
#define WAVEFRONTREDUCE 256
#define CLFASTTHRESHOLD 100000
#define RULEPROBDECT 0.99
#define MISSVAL INT32_MIN
#define MAXVAL INT32_MAX
#define MINVAL (INT32_MIN+1)
#define MINFVAL FLT_MIN
#define MAXFVAL FLT_MAX

enum min_max_t {MIN, MAX, UNKNOWN};

struct cond_t {
	size_t inp;
	int32_t min;
	int32_t max;
};

/**************************************************************************

	Section with externing OpenCl env variable

**************************************************************************/

#ifdef OPENCL
#include "timing.h"
#include "cl-helper.h"
extern char growing, fastcl;
extern cl_context ctx;
extern cl_command_queue queue;
extern cl_device_id device;
extern long long devtype;
extern cl_uint computeunits;
extern cl_mem cled, cllen, clvres, clvsum, clvind;
extern cl_kernel knl_ReduceTwoStep;
extern size_t oldlen;
extern size_t ldim[1];
extern size_t gdim[1];
extern cl_kernel *knl_VChunk, *knl_VChunkAdd;
extern char *knl_text;
extern cl_mem **clchunks;
#define COVERED(A,B,C,D,E,F) covered_cl(A,B,C,D,E,F)
#define COVEREDFAST(A,B,C,D,E,F,G,H) covered_cl(A,B,C,D,E,G,H)
#define DROPPOINTS(A,B,C,D,E,F) drop_points_cl(A,B,C,D,E,F)
#else
#define COVERED(A,B,C,D,E,F) covered(A,C,D,E,F)
#define COVEREDFAST(A,B,C,D,E,F,G,H) covered_fast(A,C,D,E,F,G,H)
/* #define COVEREDFAST(A,B,C,D,E,F,G) covered(A,C,D,E,F,G) */
#define DROPPOINTS(A,B,C,D,E,F) drop_points(A,B,C,D,E,F)
#endif

/**************************************************************************

	Functions in pmc_mapred.cpp

**************************************************************************/

#ifndef HADOOP
void pmc_map(char *charp);
#endif

#endif
