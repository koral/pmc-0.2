/**
 * @file		pmc_learn.h
 * @date		Aug 29 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *			David Vesely <veselda7@gmail.com>
 *
 * @brief		Header file for external learning loop functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_learn_h_included_
#define _pmc_learn_h_included_

#include "pmc_core.h"
#include "pmc_rules.h"

/* To communicate with parse_opt. */
struct arguments_t {
	char *args[1]; /* arg1  */
	char autodim;
	int verbose, interactive, silent, ncondmax, ncondmin, longname;
	double startcovering, mincov, maxerrperc, validperc, probability;
	char *outfile;
};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Function for generating the pmc model.
 *
 * @param[in] data		Pointer to the struct data_set_t to use.
 * @param[in] cov		Starting accettable covering.
 * @param[in] mincov		Minimum accettable covering in learning process.
 * @param[in] arguments		arguments_t struct carrying arguments.
 * @param[in,out] errmaxv	Array containing the max number of pattern
 *				macchable as error for every class.
 * @param[in] ncondmax		Max number of condition.
 * @param[in] ncondmin		Min number of condition.
 * @param[in] autodim		Autodim detection flag.
 *
 * @return
 */
extern struct edges_collector_t *learn_pmc_model(const struct data_set_t *data,
						double cov,
						const double mincov,
						const struct arguments_t
						arguments,
						size_t *errmaxv,
						const size_t ncondmax,
						const size_t ncondmin,
						const char autodim);

/**
 * Print rules to a stream.
 *
 * @param[in,out] out_stream	Stream to use.
 * @param[in] silent		Silent behaviour.
 * @param[in] nout		Output classes number.
 * @param[in] edcoll		Struct edges_collector_t by reference in which
 *				rules are stored.
 * @param[in] names		Features names.
 * @param[in] types		Featurs types.
 */
extern void fprint_rules(FILE *out_stream, int silent, size_t nout,
			struct edges_collector_t *edcoll, char_array_t names,
			char *types);

/**
 * Print classification results to a stream.
 *
 * @param[in,out] out_stream	Stream to use.
 * @param[in] silent		Silent behaviour.
 * @param[in] validperc		Validation percentage
 * @param[in] nelem		Number of patterns in the dataset.
 * @param[in] errtr		Classification error on the dataset.
 * @param[in] errval		Classification error on the validationset.
 */
extern void fprintf_results(FILE* out_stream, int silent, double validperc,
			size_t nelem, size_t errtr, size_t errval);

#endif
