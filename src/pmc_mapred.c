/**
 * @file		pmc_mapred.c
 * @date		Nov 8 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Map Reduce algorithm interface
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <alloca.h>
#include "pmc_ind.h"
#include "pmc_core.h"
#include "pmc_dataset.h"
#include "pmc_utils.h"
#include "pmc_rules.h"

/**
 * Mapping function for pmc algo.
 *
 * @param charp 	pointer to the string buffer to use as paramenters
 * for mapping
 */
void pmc_map(char *charp)
{
	int32_t nout;
	double mincov, maxerrperc, validperc;
	size_t i, len_comb; /* nrow; */
	char_array_t names;
	size_t *comb, *errmaxv, *noutclass, *fakecomb;
	struct rules_t *rules;
	char **cindext;
	struct data_set_t data;
	struct edges_collector_t *edcoll = NULL;

	/* int a = 1; */

	/* while(a) */
	/*   ; */

	printf("%s", charp);
	/* nrow = atol(charp); */
	shift_char_p(&charp);
	nout = atoi(charp);
	shift_char_p(&charp);
	len_comb = atol(charp);
	shift_char_p(&charp);
	comb = alloca(len_comb * sizeof(*comb));
	for (i = 0; i < len_comb; i++) {
		comb[i] = atol(charp);
		shift_char_p(&charp);
	}
	noutclass = alloca(len_comb * sizeof(*noutclass));
	for (i = 0; i < nout; i++) {
		noutclass[i] = atol(charp);
		shift_char_p(&charp);
	}
	maxerrperc = atof(charp);
	shift_char_p(&charp);
	mincov = atof(charp);

	load_data_form_cache(&data, comb, len_comb, nout);

	rules = init_rules(nout);
	init_char_array(&names);

	errmaxv = alloca(nout * sizeof(*errmaxv));
	validperc = 100 * (double)(data.c[0][0]->len - data.c[0][0]->trainlen) /
		data.c[0][0]->len;
	for (i = 0; i < nout; i++)
		errmaxv[i] = (size_t)(maxerrperc * (noutclass[i] / 100) *
				((100 - validperc) / 100));

	edcoll = resize_edges_collector(NULL, EDSTEP);
	fakecomb = alloca(len_comb * sizeof(*fakecomb));
	for (i = 0; i < len_comb; i++)
		fakecomb[i] = i;

	cindext = load_ind_from_cache(&data);
	find_rules(&data, fakecomb, mincov, &edcoll, len_comb, errmaxv, cindext);
	free_chunks_ind(cindext, &data);

	/* put the right comb in the rules */
	for (i = 0; i < edcoll->len; i++)
		memcpy(edcoll->ed[i]->comb, comb, len_comb * sizeof(*comb));

	for (i = 0; i < edcoll->len; i++) {
		char *buf;
		struct edges_t *ed;

		ed = edcoll->ed[i];
		buf = print_rules(ed, fakecomb, NULL, NULL);
		append_rule(buf, &rules[ed->nclass]);
	}

	/* log rules */
#ifdef HADOOP
	hadoop_emit_rule(rules, comb, len_comb, nout);
#else
	log_rules_to_hd(rules, comb, len_comb, nout);
#endif
	free_edges_collector(edcoll);
	free_rules(rules, nout);
	free_edges(data.limits);
	free_chunks(data.c, len_comb, nout);

	return;
}
