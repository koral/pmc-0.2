/**
 * @file		pmc_rules.c
 * @date		Sept 26 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for rules_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include "pmc_rules.h"
#include "pmc_utils.h"

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern struct rules_t *init_rules(int32_t nout)
{
	int32_t i;
	struct rules_t *rules;

	rules = malloc(nout * sizeof(*rules));
	if (!rules)
		error();
	for (i = 0; i < nout; i++) {
		rules[i].maxlen = rules[i].len = 0;
		rules[i].nclass = NULL;
		rules[i].ncond = NULL;
		rules[i].cov = NULL;
		rules[i].err = NULL;
		rules[i].v = NULL;
		resize_rules(&rules[i], STRINGSTEP);
	}

	return rules;
}

extern void free_rules(struct rules_t *rules, int32_t nout)
{
	int32_t i;
	size_t j;

	for (i = 0; i < nout; i++) {
		for (j = 0; j < rules[i].len; j++)
			free(rules[i].v[j]);
		free(rules[i].v);
		free(rules[i].nclass);
		free(rules[i].ncond);
		free(rules[i].cov);
		free(rules[i].err);
	}
	free(rules);

	return;
}

extern void resize_rules(struct rules_t *rules, size_t size)
{
	rules->v = realloc((void *)rules->v,
			size * (rules->maxlen + sizeof(*rules->v)));
	if (!rules->v)
		error();

	rules->nclass = realloc((void *)rules->nclass,
				size * (rules->maxlen +
					sizeof(*rules->nclass)));
	if (!rules->nclass)
		error();

	rules->ncond = realloc((void *)rules->ncond,
			size * (rules->maxlen + sizeof(*rules->ncond)));
	if (!rules->ncond)
		error();
	rules->cov = realloc((void *)rules->cov,
			size * (rules->maxlen + sizeof(*rules->cov)));
	if (!rules->cov)
		error();

	rules->err = realloc((void *)rules->err,
			size * (rules->maxlen + sizeof(*rules->err)));
	if (!rules->err)
		error();

	rules->maxlen = size;

	return;
}

extern void append_rule(char* str, struct rules_t *rules)
{

	if(rules->len == rules->maxlen)
		resize_rules(rules, STRINGSTEP);
	rules->v[rules->len++] = str;

	return;
}
