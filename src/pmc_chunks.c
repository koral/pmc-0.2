/**
 * @file		pmc_chunks.c
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Implementation file for data_chunk_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "pmc_chunks.h"

int posix_memalign(void **memptr, size_t alignment, size_t size);

/**************************************************************************

	Private functions prototypes.

**************************************************************************/

/**
 * Resize a struct data_chunk_t incrementing the size of size elements.
 * New memory is always aligned and the newly allocated is zero setted.
 *
 * @param[in] p 		pointer to struct struct data_chunk_t
 * @param[in] size 		size increment
 *
 * @return 			struct struct data_chunk_t returned
 */
static struct data_chunk_t *resize_chunk(struct data_chunk_t *p, size_t size);

/**
 * Load a data chunk from file.
 * Use in the map reduce cycle.
 *
 * @param[out] chunk_ref	data chunk by ref to allocate and fill
 * @param[in] currout		current output class number to load
 * @param[in] ncol		current varaible number to load
 */
static void load_chunk_from_cache(struct data_chunk_t **chunk_ref, int32_t currout,
			size_t ncol);
/**
 * Init data chunks index (used by InitChunks).
 *
 * @param[out] chunks_ref	data chunks by reference
 * @param[in] ncol	 	number of input variables
 * @param[in] nout		number of output classes
 */
static void init_void_chunks(struct data_chunk_t ****chunks_ref, size_t ncol,
			int32_t nout);
/**
 * Given a line return the output in string format.
 *
 * @param[in] linebuf	buffer containing the line
 * @param[in] outpos	position of the output column
 * @param[in] sepc	char separator
 *
 * @return		output in string format
 */
static char *find_el_out(char *linebuf, size_t outpos, char sepc);

/**************************************************************************

	Public functions implementation.

**************************************************************************/

extern void init_chunks(struct data_chunk_t ****chunks_ref, size_t ncol,
		size_t *noutclass, int32_t nout, char *types)
{
	size_t i, j;
	struct data_chunk_t ***chunks;

	init_void_chunks(&chunks, ncol, nout);
	for (i = 0; i < nout; i++) {
		for (j = 0; j < ncol + nout + 1; j++) {
			chunks[i][j] = NULL;
			chunks[i][j] = resize_chunk(chunks[i][j], noutclass[i]);
			if (j < ncol) {
				if (types[j] == 'f')
					chunks[i][j]->type = FLOAT;
				else
					chunks[i][j]->type = INT;
			}
			else {
				chunks[i][j]->type = INT;
			}
			chunks[i][j]->len = 0;
		}
	}
	*chunks_ref = chunks;

	return;
}

extern struct data_chunk_t ***load_chunks_from_cache(size_t len_comb,
						size_t *comb, int32_t nout)
{
	size_t i, j;
	struct data_chunk_t ***chunks;

	init_void_chunks(&chunks, len_comb, nout);
	for (i = 0; i < nout; i++)
		for (j = 0; j < len_comb; j++)
			load_chunk_from_cache(&chunks[i][j], i, comb[j]);

	return chunks;
}

extern void fill_chunks(struct data_chunk_t ***chunks, char *filebuf, size_t ncol,
			size_t nline, size_t outpos, char_array_t *outnames,
			size_t *ind)
{
	int32_t ic;
	size_t i, j, k;
	char *pline, *pel, *out;

	pel = pline = filebuf;

	for (i = 0; i < nline; i++) {
		if (i)
			pel = pline = find_new_el(pline, '\n');
		out = find_el_out(pline, outpos, '\t');
		for (j = 0; j < ncol - 1; j++) {
			k = ind[j];
			if (j)
				pel = find_new_el(pel, '\t');
			/* ic = FindIndInt32(noutclass, out, MAXCLASSOUT); */
			ic = find_el_char_array(outnames, out);
			if (ic == -1)
				error();
			if (chunks[ic][k]->type == INT)
				chunks[ic][k]->v[(chunks[ic][k]->len)++].i =
					atoi(pel);
			else
				chunks[ic][k]->v[(chunks[ic][k]->len)++].e =
					enc_f(atof(pel));
		}
		free(out);
	}

	return;
}

extern void find_chunk_max_min(struct data_chunk_t *c, int32_t *max_ref,
				       int32_t *min_ref)
{
	int32_t max, min;
	size_t i;

	min = max = c->v[0].i;
	for (i = 1; i < c->trainlen; i++) {
		if (min>c->v[i].i)
			min = c->v[i].i;
		if (max<c->v[i].i)
			max = c->v[i].i;
	}
	*max_ref = max;
	*min_ref = min;
}

extern void free_chunks(struct data_chunk_t ***chunks, size_t ncol,
			int32_t nout)
{
	size_t i, j;

	for (i = 0; i < nout; i++)
		for (j = 0; j < ncol + nout + 1; j++)
			free(chunks[i][j]);

	for (i = 0; i < nout; i++)
		free(chunks[i]);

	free(chunks);

	return;
}

/**************************************************************************

	Private functions implementation.

**************************************************************************/

static struct data_chunk_t *resize_chunk(struct data_chunk_t *p, size_t size)
{
	struct data_chunk_t *ptmp = NULL;

	if (p) {
		if (posix_memalign((void **)&ptmp, ALIGNSIZE,
					sizeof(struct data_chunk_t) +
					(size * sizeof(*ptmp->v) - 1)))
			error();
		memcpy(ptmp, p, p->len);
		free(p);
		p = ptmp;
	} else {
		if (posix_memalign((void **)&p, ALIGNSIZE,
					sizeof(struct data_chunk_t) +
					(size * sizeof(*p->v)-1)))
			error();
	}

	p->maxlen = size;
	return p;
}

static void load_chunk_from_cache(struct data_chunk_t **chunk_ref, int32_t currout,
				size_t ncol)
{
	size_t len, chunksize;
	char chunkname[MAXSTRING];
	FILE *pfile;
	struct data_chunk_t *chunk = NULL;

	sprintf(chunkname, STR(TEMPFILEDIR)
		STR(CHUNKFILENAME), currout, ncol);
	pfile = fopen(chunkname, "r");
	if (!pfile)
		error();
	if (fread(&len, sizeof(size_t), 1, pfile) != 1)
		error();
	rewind(pfile);
	chunksize = sizeof(struct data_chunk_t) + (len - 1) * sizeof(int32_t);
	chunk = resize_chunk(chunk, chunksize);
	if (fread(chunk, chunksize, 1, pfile) != 1)
		error();
	fclose(pfile);
	*chunk_ref = chunk;
}

static void init_void_chunks(struct data_chunk_t ****chunks_ref, size_t ncol,
		int32_t nout)
{
	size_t i;
	struct data_chunk_t ***chunks;

	chunks = malloc(nout * sizeof(*chunks));
	for (i = 0; i < nout; i++)
		chunks[i] = calloc(1,
				(ncol + nout + 1)* sizeof(**chunks));
	*chunks_ref = chunks;

	return;
}

static char *find_el_out(char *linebuf, size_t outpos, char sepc)
{
	size_t i;
	char chtmp;
	char *str;

	if (outpos)
		for (i = 0; i < outpos - 1; i++)
			linebuf = find_new_el(linebuf, sepc);

	for (i = 0; linebuf[i] != sepc && linebuf[i] != '\n'; i++)
		;
	chtmp = linebuf[i];
	linebuf[i] = '\0';
	str = malloc(MAXSTRING);
	strcpy(str, linebuf);
	linebuf[i] = chtmp;

	return str;
}
