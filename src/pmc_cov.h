/**
 * @file		pmc_cov.h
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for external covering functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_cov_h_included_
#define _pmc_cov_h_included_

#include "pmc_chunks.h"

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Optimized function for covering computation.
 * Return 1 if covering >= errmax or 0.
 *
 * @param[in] nout		output classes number
 * @param[in] edges		struct edges_t carrying the rule
 * @param[in] inds		vector with the variable indexes used by the
 *				edge
 * @param[in] ninds		number of variable in the combo inds
 * @param[in] errmax		max number of pattern covered
 * @param[in] chunks		chunk data matrix
 * @param[out] count_ref	pattern covered
 *
 * @return			1 if covering >= errmax or 0
 */
extern char covered_fast(register int32_t nout, register struct edges_t *edges,
				 register size_t *inds, register size_t ninds,
				 register size_t errmax, register struct data_chunk_t ***chunks,
				 size_t *count_ref);
/**
 * Optimized function for covering computation.
 * Same as CoveredFast but without the covered pattern limit.
 *
 * @param[in] nout	output classes number
 * @param[in] edges	struct edges_t carrying the rule
 * @param[in] inds	vector with the variable indexes used by the
 *			edge
 * @param[in] ninds	number of variable in the combo inds
 * @param[in] chunks	chunks data matrix
 *
 * @return		number of pattern covered
 */
extern size_t covered(register int32_t nout, register struct edges_t *edges,
			      register size_t *inds, register size_t ninds,
			      register struct data_chunk_t ***chunks);

#endif
