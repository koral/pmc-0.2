/**
 * @file		pmc_lexer.l
 * @date		Dec 4 2013
 * @copyright		(C) 2013 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Lexer to parse rules in text format
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

%{
#define YY_NO_INPUT
#include "pmc_base.h"
#include "pmc_parser.h"
#include "pmc_utils.h"

void yyerror(char const *);
int yyparse (void);
char *strdup(const char *s);
int fileno(FILE *stream);
%}

%option nounput
%option yylineno

%%

"="             return EQ;
"<="            return LE;
"%"             return PERC;
"OUT"           return OUT;
"Cov"           return COV;
"Err"           return ERR;

[0-9]+"."[0-9]+ {
	yylval.f = (float)atof(yytext);
	return FLOATING;
}

[0-9]+          {
	yylval.i = atoi(yytext);
	return INTEGER;
}

[A-Za-z#_]+  {
	yylval.s = strdup(yytext);
	return STRING;
}

[\n]      return *yytext;

[ \t]     ;       /* skip whitespace */

.         yyerror("Unknown character");

%%

int yywrap(void)
{
	return 1;
}

int parse(char *filename)
{
	FILE *f;

	if (filename) {
		f = fopen(filename, "r");
		if (!f) {
			printf("Cannot open file\n");
			error();
			return -1;
		}
		yyin = f;
	}
	yyparse();

	fclose(f);

	return 0;
}
