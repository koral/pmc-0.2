/**
 * @file		pmc_api.h
 * @date		Feb 8 2015
 * @copyright		(C) 2015 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for the pmc API.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_api_h_included_
#define _pmc_api_h_included_

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Function for generating the pmc model.
 *
 * @param[in] data_buf		Pointer to the char buffer containing
 *				the dataset.
 * @param[in] validperc		Percentage tu use as validation set.
 * @param[in] cov		Starting accettable covering.
 * @param[in] maxerrperc	Max error allowed for rule generation in
 *				percentage.
 * @param[in] mincov		Minimum accettable covering in learning process.
 * @param[in] ncondmax		Max number of condition.
 * @param[in] ncondmin		Min number of condition.
 * @param[in] autodim		Autodim detection flag.
 *
 * @return
 */
extern struct edges_collector_t *pmc_api_learn_model(char *data_buf,
						double validperc,
						double cov,
						double maxerrperc,
						const double mincov,
						const size_t ncondmax,
						const size_t ncondmin,
						const char autodim);
/**
 * Allocate and initialize the max error allowed array.
 *
 * @param[in] nout		Output classes number.
 * @param[in] noutclass		Vector with the number of elements for every.
 * @param[in] maxerrperc	Max error allowed for rule generation in
 *				percentage.
 *
 * @return			pointer to the max error allowed.
 */
extern size_t *init_err_max(size_t nout, size_t *noutclass, double maxerrperc);

#endif
