/**
 * @file		pmc_comb.h
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for data_chunk_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_comb_h_included_
#define _pmc_comb_h_included_

#include "pmc_base.h"
#include "pmc_char_array.h"

/**************************************************************************

	Public structure declarations.

**************************************************************************/

/** Structure for holding an array containing the feature index. */
struct comb_t {
	/** Number of indexes stored. */
	uint32_t len;
	/** Pointer to the feature index array. */
	size_t *comb;
};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Generate all the permutation without repetition of k groups in n.
 * Permutation generator originaly from
 * http://dzone.com/snippets/generate-combinations
 *
 * @param[out] combs_ref	permutation matrix by reference
 * @param[out] len_comb_ref	number of permutations by reference
 * @param[in] n			number of elements
 * @param[in] k			number of extraction
 */
extern void gen_combs(size_t ***combs_ref, size_t *len_comb_ref, size_t n, size_t k);

/**
 * Free the combination matrix.
 *
 * @param[in,out] combs		the combination matrix to free
 * @param[in] len_comb		the number of cominations in the matrix
 */
extern void free_combs(size_t **combs, size_t len_comb);

#endif
