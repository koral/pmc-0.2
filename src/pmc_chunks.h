/**
 * @file		pmc_chunks.h
 * @date		Aug 30 2014
 * @copyright		(C) 2014 Andrea Corallo
 * @contributors	Andrea Corallo <andrea_corallo@yahoo.it>
 *
 * @brief		Header file for data_chunk_t related functions.
 *
 * @license		GPL version 3
 *
 * This file is part of PMC.
 *
 * PMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PMC.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _pmc_chunks_h_included_
#define _pmc_chunks_h_included_

#include "pmc_utils.h"

/**************************************************************************

	Public structure declarations.

**************************************************************************/

/** Structure for holding a single contiguous chunk of data in memory. */
struct data_chunk_t {
	/** Actual lenght of the chunk in elements. */
	size_t len;
	/** Number of elements to use as training set. */
	size_t trainlen;
	/** Actual allocated lenght for the chunk */
	size_t maxlen;
	/** Type of data stored in the chunk. */
	enum data_type_t type;
	/** Variable lenght array containing the data. */
	union elem_32_t v[1];
};

/**************************************************************************

	Public functions prototypes.

**************************************************************************/

/**
 * Init data chunks.
 *
 * @param[out] chunks_ref	data chunks by reference
 * @param[in] ncol		number of input variables
 * @param[in] noutclass		vector with the number of elements for every
 *				class
 * @param[in] nout		number of output classes
 * @param[in] types		vector with types
 */
extern void init_chunks(struct data_chunk_t ****chunks_ref, size_t ncol,
			size_t *noutclass, int32_t nout, char *types);

/**
 * Load chuncks data segment from hadoop cache file.
 *
 * @param[in] len_comb	Number of feature used in the current combination.
 * @param[in] comb	Current combination of feature.
 * @param[in] nout	Number of output classes.
 *
 * @return
 */
extern struct data_chunk_t ***load_chunks_from_cache(size_t len_comb,
						size_t *comb, int32_t nout);

/**
 * Fill the chunk data matrix with the data.
 *
 * @param[in,out] chunks	chunk data matrix to fill
 * @param[in] filebuf		input text buffer
 * @param[in] ncol		number of column (varaibles!?) in the buffer
 * @param[in] nline		number of line in the text buffer
 * @param[in] outpos		index position of the output
 * @param[in] outnames		char_array_t containing the output class names
 * @param[in] ind		position variable index array
 */
extern void fill_chunks(struct data_chunk_t ***chunks, char *filebuf, size_t ncol,
			size_t nline, size_t outpos, char_array_t *outnames,
			size_t *ind);
/**
 * Find the maximum and the minimum for data in a struct data_chunk_t
 *
 * @param[in] c			struct data_chunk_t in input
 * @param[out] max_ref		pointer to the max value to set
 * @param[out] min_ref		pointer to the min value to set
 */
extern void find_chunk_max_min(struct data_chunk_t *c, int32_t *max_ref, int32_t *min_ref);

/**
 * Free the main data matrix.
 *
 * @param[in,out] chunks	data chunks
 * @param[in] ncol		input variables numbers
 * @param[in] nout		number of output classes
 */
extern void free_chunks(struct data_chunk_t ***chunks, size_t ncol, int32_t nout);

#endif
