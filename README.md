pmc-0.2
=======

Parallel Multivariate Classifier is an experimental algorithm created with this aim:

Having a tool able to process huge quantity of data solving classification and regression problems in a real multivariate way in big data environment.

The output of the algorithm is a set of intelligible rules that connects inputs to outputs.

Rules are really good to build non black-box models and to explore the knowledge inside the data.

PMC (Parallel Multivariate Classifier) is the implementation for the actual classification tool.

It is written in C for performance reason and is designed to runs on:

* CPU
* GPU (OpenCL)
* CPU Cluster (Hadoop infrastructure)
* Hybrid CPU GPU Cluster (Hadoop OpenCl infrastructure)

Single CPU compile instructions and basic usage:

make

./bin/pmc datasets/2dchess_1000p_1000v_10dim_0err.float

./bin/pmc datasets/3dchess_1000p_1000v_10dim_0err.float

Official webpage for PMAlgo project at http://www.pmalgo.org
