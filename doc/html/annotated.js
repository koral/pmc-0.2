var annotated =
[
    [ "arguments_t", "structarguments__t.html", "structarguments__t" ],
    [ "char_array_t", "structchar__array__t.html", "structchar__array__t" ],
    [ "comb_t", "structcomb__t.html", "structcomb__t" ],
    [ "cond_t", "structcond__t.html", "structcond__t" ],
    [ "data_chunk_t", "structdata__chunk__t.html", "structdata__chunk__t" ],
    [ "data_set_t", "structdata__set__t.html", "structdata__set__t" ],
    [ "edges_collector_t", "structedges__collector__t.html", "structedges__collector__t" ],
    [ "edges_t", "structedges__t.html", "structedges__t" ],
    [ "elem_32_t", "unionelem__32__t.html", "unionelem__32__t" ],
    [ "encoded_float_t", "structencoded__float__t.html", "structencoded__float__t" ],
    [ "PmcMapper", "classPmcMapper.html", "classPmcMapper" ],
    [ "PmcReducer", "classPmcReducer.html", "classPmcReducer" ],
    [ "rules_t", "structrules__t.html", "structrules__t" ],
    [ "yy_buffer_state", "structyy__buffer__state.html", "structyy__buffer__state" ],
    [ "yy_trans_info", "structyy__trans__info.html", "structyy__trans__info" ],
    [ "yyalloc", "unionyyalloc.html", "unionyyalloc" ],
    [ "YYSTYPE", "unionYYSTYPE.html", "unionYYSTYPE" ]
];