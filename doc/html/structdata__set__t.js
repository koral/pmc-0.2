var structdata__set__t =
[
    [ "c", "structdata__set__t.html#a90c20c12d78b31193dd72665bfe19f6d", null ],
    [ "limits", "structdata__set__t.html#affbbb779aa325500308c90a03fe40265", null ],
    [ "names", "structdata__set__t.html#ac931a3bd3d56eb59b5434b48ac40ebb3", null ],
    [ "ncol", "structdata__set__t.html#afd941b79af9bf3b1600690582841c401", null ],
    [ "nelem", "structdata__set__t.html#aa561f15891a7fadac618a530aee51026", null ],
    [ "nout", "structdata__set__t.html#a39c622a1c0bcc6dfde43b3d3debe06c3", null ],
    [ "nout_class", "structdata__set__t.html#a6c9302c17087504f49fc92a081db570d", null ],
    [ "out", "structdata__set__t.html#a707603cfd00efedfe88e855b535084c6", null ],
    [ "types", "structdata__set__t.html#af79738f7f2b773fb01f53273c1609c4f", null ]
];