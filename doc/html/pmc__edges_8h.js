var pmc__edges_8h =
[
    [ "edges_t", "structedges__t.html", "structedges__t" ],
    [ "edges_collector_t", "structedges__collector__t.html", "structedges__collector__t" ],
    [ "append_ed", "pmc__edges_8h.html#a1012b5795d858199051c23d0efcdc370", null ],
    [ "free_edges", "pmc__edges_8h.html#ac1bd179b2e1e8cdd8227f9e11d2f175a", null ],
    [ "free_edges_collector", "pmc__edges_8h.html#ab245c36460879957df3705942c7981ca", null ],
    [ "init_edges", "pmc__edges_8h.html#aec04bf570e8021e325ebe9caa17dcc05", null ],
    [ "resize_edges_collector", "pmc__edges_8h.html#ac6bd071ad42e4de16400bae5e81886f2", null ]
];