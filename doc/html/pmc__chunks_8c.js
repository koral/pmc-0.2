var pmc__chunks_8c =
[
    [ "fill_chunks", "pmc__chunks_8c.html#a9836c18ccdc568d16bf48b0a340bc721", null ],
    [ "find_chunk_max_min", "pmc__chunks_8c.html#a076331386a9be0539472c024ef4dbdf4", null ],
    [ "find_el_out", "pmc__chunks_8c.html#a8424d950a39794602b3b34b55c58ae75", null ],
    [ "free_chunks", "pmc__chunks_8c.html#a6eb16e58b35b76f1fc7b2c09e6231f24", null ],
    [ "init_chunks", "pmc__chunks_8c.html#a6883bbb58be472a87b370f193215aed1", null ],
    [ "init_void_chunks", "pmc__chunks_8c.html#a9c60c1dc5d2ee94355ff16e9201bb8ca", null ],
    [ "load_chunk_from_cache", "pmc__chunks_8c.html#aa33cfb56561d085dbe782da7818c52b8", null ],
    [ "load_chunks_from_cache", "pmc__chunks_8c.html#af0c545fcda7734f631df86575affc494", null ],
    [ "posix_memalign", "pmc__chunks_8c.html#aed1e9c27ea92f5ae3a480059bf45c311", null ],
    [ "resize_chunk", "pmc__chunks_8c.html#a0533fd6b9b7d860f4a23e44aff7005d9", null ]
];