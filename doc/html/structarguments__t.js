var structarguments__t =
[
    [ "args", "structarguments__t.html#aebd51662c78b8bacadcaf05b01478f2c", null ],
    [ "autodim", "structarguments__t.html#a10bcdacca83a79493254f75ad01f0c2c", null ],
    [ "infile", "structarguments__t.html#a1bca4e6e2253bb19f69879c73dbc20ca", null ],
    [ "interactive", "structarguments__t.html#a9cf81bb147be11120970522ff820fac9", null ],
    [ "longname", "structarguments__t.html#a1d10a62e097c1acd9b38bf733f3e9731", null ],
    [ "maxerrperc", "structarguments__t.html#a5d156dbd86cd179292fd8e2426190bc2", null ],
    [ "mincov", "structarguments__t.html#ae38351f2e41b90d4c077e44b465041a2", null ],
    [ "miss", "structarguments__t.html#a4ae3a89e9db68606dbaa228cb585d22e", null ],
    [ "names", "structarguments__t.html#abc9705bc89d96a5c8b82166f278b7a99", null ],
    [ "ncondmax", "structarguments__t.html#a85f803b288732c8545b45074b5171112", null ],
    [ "ncondmin", "structarguments__t.html#a1898721b1ef1af5b4eed33fe9597f93c", null ],
    [ "outfile", "structarguments__t.html#a1458bd96c5ece8a09345196a872b5459", null ],
    [ "probability", "structarguments__t.html#a0fa1216ce704e11abd1cc7a79e8b1175", null ],
    [ "sep", "structarguments__t.html#a3a954af9467f862df22cb7cf65546fc8", null ],
    [ "silent", "structarguments__t.html#a7f1b50021ee96b43729b6eb757c66bdb", null ],
    [ "startcovering", "structarguments__t.html#ad996c6d4eb190a4db7bf0869eae8e625", null ],
    [ "validperc", "structarguments__t.html#ab8db2f831e4f3446f45cd2da5c5b5d3b", null ],
    [ "verbose", "structarguments__t.html#ac974951e02a3cbd056c3f7b9d3688f17", null ]
];