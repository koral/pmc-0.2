var pmc__datatype_8h =
[
    [ "encoded_float_t", "structencoded__float__t.html", "structencoded__float__t" ],
    [ "elem_32_t", "unionelem__32__t.html", "unionelem__32__t" ],
    [ "data_type_t", "pmc__datatype_8h.html#a49b5dfea31023fcd165ce36384234dc5", [
      [ "INT", "pmc__datatype_8h.html#a49b5dfea31023fcd165ce36384234dc5afd5a5f51ce25953f3db2c7e93eb7864a", null ],
      [ "FLOAT", "pmc__datatype_8h.html#a49b5dfea31023fcd165ce36384234dc5a9cf4a0866224b0bb4a7a895da27c9c4c", null ],
      [ "ENCODED_FLOAT", "pmc__datatype_8h.html#a49b5dfea31023fcd165ce36384234dc5a8ec1a4e37d52cb0a751f6e52fff1a395", null ]
    ] ],
    [ "dec_f", "pmc__datatype_8h.html#abb17aa03a66282998d8426e7ccaa924a", null ],
    [ "enc_f", "pmc__datatype_8h.html#aaae61676fa5d2f69a7836b4490f010e5", null ]
];