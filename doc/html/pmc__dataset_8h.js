var pmc__dataset_8h =
[
    [ "data_set_t", "structdata__set__t.html", "structdata__set__t" ],
    [ "free_data_set", "pmc__dataset_8h.html#afec515c0bb32d02bac5c218f2c590157", null ],
    [ "load_data_form_cache", "pmc__dataset_8h.html#a459b2d201a73e2cc5851edfa711073af", null ],
    [ "load_data_from_buf", "pmc__dataset_8h.html#a63c774ee4bc5ab6ea3b8ddca90f4a177", null ],
    [ "load_data_from_file", "pmc__dataset_8h.html#a78faf6d2b712b65a22c2e7e79f047322", null ],
    [ "load_heterogeneous_file", "pmc__dataset_8h.html#a0eb7a8aecb91387c04dd9758feab9ea8", null ],
    [ "set_validation_perc", "pmc__dataset_8h.html#a5010ebe61678d9f6366b5145524c1677", null ]
];